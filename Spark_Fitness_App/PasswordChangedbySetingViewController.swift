//
//  PasswordChangedbySetingViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 27/12/21.
//

import UIKit

class PasswordChangedbySetingViewController: UIViewController {
    @IBOutlet var view_pop1: UIView!
    @IBOutlet weak var view_PopUpMessage: UIView!{didSet{
        view_PopUpMessage.layer.cornerRadius = 10
    }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func CloseBtn_Action(_ sender: Any) {
        view_pop1.removeFromSuperview()
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    
}
