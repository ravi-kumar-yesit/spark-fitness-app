//
//  GroupsViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 27/12/21.
//

import UIKit

class GroupsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var imageArray = ["Img1-1","img2-1","img3-1","img4-1","img5-1","img6-1","img7-1"]
    var groupNameArray = ["Groups Name","Groups Name","Groups Name","Groups Name","Groups Name","Groups Name","Groups Name"]
    var groupDescriptionArray = ["Dummy text Lorem ipsumdolor sit conuse sectetuer amet","Dummy text Lorem ipsumdolor sit conuse sectetuer amet","Dummy text Lorem ipsumdolor sit conuse sectetuer amet","Dummy text Lorem ipsumdolor sit conuse sectetuer amet","Dummy text Lorem ipsumdolor sit conuse sectetuer amet","Dummy text Lorem ipsumdolor sit conuse sectetuer amet","Dummy text Lorem ipsumdolor sit conuse sectetuer amet"]
    
    @IBOutlet weak var DownloadTblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        cell.contentView.layer.cornerRadius = 5
//        cell.contentView.layer.masksToBounds = true
        self.tabBarController?.hidesBottomBarWhenPushed = true
        self.hidesBottomBarWhenPushed = true
        self.tabBarController?.tabBar.isHidden = true
       // self.tabBarController?.tabBar.ishidden = false
      //  GroupsViewController.hidesBottomBarWhenPushed = true

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DownloadTblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GroupsTableViewCell
        cell.groupImg.image = UIImage(named: imageArray[indexPath.row])
        cell.groupTitle.text = groupNameArray[indexPath.row]
        cell.groupDescription.text = groupDescriptionArray[indexPath.row]
        
        cell.view_tableData.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        return cell
    }
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == UITableViewCell.EditingStyle.delete {
//                exerciseArray.remove(at: indexPath.row)
//            imageArray.remove(at: indexPath.row)
//                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
//            DownloadTblView.reloadData()
//        }}
    
    @IBAction func ThreeDotBtn_Action(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: DownloadTblView)
        guard let indexpath = DownloadTblView.indexPathForRow(at: point) else {
            return}
        imageArray.remove(at: indexpath.row)
        //exerciseArray.remove(at: indexpath.row)
        DownloadTblView.deleteRows(at: [IndexPath(row: indexpath.row, section: 0)], with: .left)
        DownloadTblView.endUpdates()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "GroupDetailsViewController") as! GroupDetailsViewController
           self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        if indexPath.row == 1 {
            let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "GroupDetailsViewController") as! GroupDetailsViewController
           self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
    
    @IBAction func backButton_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
