//
//  GroupDetailsViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 29/12/21.
//

import UIKit

class GroupDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var groupdetailetblView: UITableView!
    var NameArray = ["Sophia","Arizona States Groups"]
      var imgArray = ["1a","1a"]
      var DescriptionArray = ["Lorem ipsum dolor sit amet consectetuer.","Lorem ipsum dolor sit amet consectetuer."]
      var arrayTime = ["5h","8h"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 270
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = groupdetailetblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GroupDetailsTableViewCell
        cell.nameLbl.text = NameArray[indexPath.row]
        cell.img.image = UIImage(named: imgArray[indexPath.row])
        cell.DescriptionLbl.text = DescriptionArray[indexPath.row]
        cell.timeLbl.text = arrayTime[indexPath.row]
        return cell
        
        
    }

    @IBAction func backBtn_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
