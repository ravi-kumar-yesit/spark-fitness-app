//
//  GoalDetailsViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 24/12/21.
//

import UIKit

class GoalDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var view_MarkasCompleted: UIView! {didSet{
        
        view_MarkasCompleted.layer.cornerRadius = view_MarkasCompleted.layer.frame.height/2
        view_MarkasCompleted.layer.borderWidth = 0.5
        view_MarkasCompleted.layer.borderColor = UIColor.gray.cgColor
    }
    }
    
    
    var DescriptionArray = ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis."]
    
    @IBOutlet weak var GoalDetailsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DescriptionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = GoalDetailsTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GoalDetailsTableViewCell
        cell.descriptionLbl.text = DescriptionArray[indexPath.row]
        return cell
    }

    @IBAction func BackBtn_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backBtn_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
