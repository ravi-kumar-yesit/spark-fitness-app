//
//  DesignView.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 05/01/22.
//

import Foundation
import UIKit
class DesignView:UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var shodowColor: UIColor? = UIColor.black
    
    @IBInspectable let shadowoffWidth: Int = 0
    @IBInspectable let shadowoffHeight: Int = 1
    
    @IBInspectable var shadowOpacity: Float = 0.2
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shodowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowoffWidth, height: shadowoffHeight)
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.shadowPath = shadowPath.cgPath
        layer.shadowOpacity = shadowOpacity
    }
    
    
    
}
