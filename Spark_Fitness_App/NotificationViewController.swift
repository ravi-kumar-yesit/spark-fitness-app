//
//  NotificationViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 27/12/21.
//

import UIKit

class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
 
    @IBOutlet weak var NotificationTblView: UITableView!
    var arrayTitle = ["Fitness","Dummy text","Dummy text","Dummy text","Dummy text"]
    var descriptionArray = ["Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor."]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationTblView.separatorColor = hexStringToUIColor(hex: "353849")
        
//        NotificationTblView.frame = NotificationTblView.frame.inset(by: UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0))
        // Do any additional setup after loading the view.
    }
    override func viewWillLayoutSubviews() {
        self.NotificationTblView.reloadData()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//            return 20
//        }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 20
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
            let cell = NotificationTblView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
            cell.titleLbl.text = arrayTitle[indexPath.row]
            cell.descriptionLbl.text = descriptionArray[indexPath.row]
        
        if indexPath.row == 0 {
            cell.view_TableData.backgroundColor = hexStringToUIColor(hex: "353849") }
        if indexPath.row == 2 {
            cell.view_TableData.backgroundColor = hexStringToUIColor(hex: "353849") }
        
        cell.view_TableData.layer.cornerRadius = 5
        cell.view_TableData.layer.masksToBounds = true
        
            return cell

    }
//
//        let cell = NotificationTblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationTableViewCell
//        cell.titleLbl.text = arrayTitle[indexPath.row]
//        cell.descriptionLbl.text = descriptionArray[indexPath.row]
//        if indexPath.row == 0 {
//            cell.view_tableData.backgroundColor = hexStringToUIColor(hex: "353849")}
        //
        //cell.contentView.backgroundColor = hexStringToUIColor(hex: "353849")
//        let backGroundView = UIView()
//        backGroundView.backgroundColor = UIColor.orange
//        cell.selectedBackgroundView = backGroundView
        
        //cell.selectionStyle = .none
        //cell.selectedBackgroundView = hexStringToUIColor(hex: "353849")
        
     // view_tableData.backgroundColor = hexStringToUIColor(hex: "353849")
        
//        let bgColorView = UIView()
//        bgColorView.backgroundColor = hexStringToUIColor(hex: "353849")
//        cell.selectedBackgroundView = bgColorView
   
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                    green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                    blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                    alpha: CGFloat(1.0)
        )
    }
  
//     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
//    {
//        let selectionColor = UIView() as UIView
//        selectionColor.layer.borderWidth = 1
//        selectionColor.layer.borderColor = UIColor.blue.cgColor
//        selectionColor.backgroundColor = UIColor.blue
//        cell.selectedBackgroundView = selectionColor
//    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell:UITableViewCell = NotificationTblView.cellForRow(at: indexPath)!

        //selectedCell.contentView.backgroundColor = hexStringToUIColor(hex: "353849")
       // tableView.deselectRow(at: indexPath, animated: true)
//    view_tableData.backgroundColor = hexStringToUIColor(hex: 353849)
      let a = 10
        let b = a+10
        print(b)
    }
    @IBAction func backBtn_Action(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func BtnclearAllNotification_Action(_ sender: UIButton) {
        
        arrayTitle.removeAll()
        descriptionArray.removeAll()
        NotificationTblView.reloadData()
      
    }
}

