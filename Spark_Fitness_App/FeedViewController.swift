//
//  FeedViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 19/12/21.
//

import UIKit

class FeedViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var NameArray = ["Sophia","Arizona States Groups"]
    var imgArray = ["Rectangle 42","Rectangle 42"]
    var DescriptionArray = ["Lorem ipsum dolor sit amet consectetuer.","Lorem ipsum dolor sit amet consectetuer."]
    var arrayTime = ["5h","8h"]
    
    @IBOutlet weak var profileImg: UIImageView!
    var dataArray = [["name":"Sophia","image":"Rectangle 42","Description":"Lorem ipsum dolor sit amet consectetu.","time":"5h","profileImage":"Rectangle 42"],["name":"Sophia","image":"Rectangle 42","Description":"Lorem ipsum dolor sit amet consectetu.","time":"5h","profileImage":"Rectangle 42"]]
    @IBOutlet weak var feedTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
        
//        return NameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = dataArray[indexPath.row]
        
        let cell = feedTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FeedTableViewCell
        
        cell.nameLbl.text = data["name"]
        cell.DescriptionLbl.text = data["Description"]
        cell.timeLbl.text = data["time"]
        cell.img.image = UIImage(named: data["profileImage"]!)
        
        
//        cell.nameLbl.text = NameArray[indexPath.row]
//        cell.DescriptionLbl.text = DescriptionArray[indexPath.row]
//        cell.timeLbl.text = arrayTime[indexPath.row]
        //        if indexPath.item == 0 {
        //            let SVC = self.storyboard?.instantiateViewController(withIdentifier: "PostFeedViewController") as! PostFeedViewController
        //
        //            SVC.name = NameArray[indexPath.row]
        //            SVC.image = UIImage(named: imgArray[indexPath.row])!
        //        self.navigationController?.pushViewController(SVC, animated: true)
        //    }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = dataArray[indexPath.row]
        
            let SVC = self.storyboard?.instantiateViewController(withIdentifier: "AnotherUserProfileViewController") as! AnotherUserProfileViewController
     // SVC.dataDict1 = data
        self.navigationController?.pushViewController(SVC, animated: true)
        
//        self.tabBarController?.hidesBottomBarWhenPushed = true
//        self.hidesBottomBarWhenPushed = true
//        self.tabBarController?.tabBar.isHidden = true

//
//                if indexPath.row == 0 {
//                   let vc = storyboard?.instantiateViewController(withIdentifier: "PostFeedViewController") as? PostFeedViewController
//                   vc?.image = UIImage(named: imgArray[indexPath.row] )!
//                   vc?.name = NameArray[indexPath.row]
//                   navigationController?.pushViewController(vc!, animated: true)
//            }
//                else if indexPath.row == 1 {
//                    let vc = storyboard?.instantiateViewController(withIdentifier: "PostFeedViewController") as? PostFeedViewController
//                    vc?.image = UIImage(named: imgArray[indexPath.row] )!
//                    vc?.name = NameArray[indexPath.row]
//                    navigationController?.pushViewController(vc!, animated: true)
//             }
        
    }
    @IBAction func crossBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func BtnWriteapost_Action(_ sender: Any) {
   

        let SVC = storyboard?.instantiateViewController(withIdentifier: "PostFeedViewController") as? PostFeedViewController
    
        self.navigationController?.pushViewController(SVC!, animated: true)
        
    
    }}
