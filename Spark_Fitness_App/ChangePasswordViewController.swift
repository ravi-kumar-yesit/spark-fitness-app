//
//  ChangePasswordViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 27/12/21.
//

import UIKit

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var oldPassword_Txt: UITextField!{didSet {
    let redPlaceholderText = NSAttributedString(string: "Old Password",
 attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    oldPassword_Txt.attributedPlaceholder = redPlaceholderText
}
}
    @IBOutlet weak var view_OldPassword: UIView! {didSet{
        view_OldPassword.layer.cornerRadius = view_OldPassword.layer.frame.height/2
        view_OldPassword.layer.borderWidth = 1
        view_OldPassword.layer.borderColor = UIColor.gray.cgColor
      }
    }
    @IBOutlet weak var ResetBtn: UIButton! {didSet{
        ResetBtn.layer.cornerRadius = ResetBtn.layer.frame.height/2
        ResetBtn.layer.borderWidth = 1
        ResetBtn.layer.borderColor = UIColor.gray.cgColor
      }
    }
    @IBOutlet weak var confirm_NewPasswordTxt: UITextField! {
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Confirm New Password",
         attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            confirm_NewPasswordTxt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var newPassword_txt: UITextField! {didSet{
            let redPlaceholderText = NSAttributedString(string: "New Password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            newPassword_txt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var view_ConfirmNewPassword: UIView! {didSet{
        view_ConfirmNewPassword.layer.cornerRadius = view_ConfirmNewPassword.layer.frame.height/2
        view_ConfirmNewPassword.layer.borderWidth = 1
        view_ConfirmNewPassword.layer.borderColor = UIColor.gray.cgColor
      }
    }
    @IBOutlet weak var view_NewPassword: UIView!{didSet{
        view_NewPassword.layer.cornerRadius = view_NewPassword.layer.frame.height/2
        view_NewPassword.layer.borderWidth = 1
        view_NewPassword.layer.borderColor = UIColor.gray.cgColor
      }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.hidesBottomBarWhenPushed = true
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
   
  
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }

        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}")
        return passwordTest.evaluate(with: testStr)
    }
    
    
    @IBAction func resetBtn_Action(_ sender: Any) {
        
        if oldPassword_Txt.text!.isEmpty  {
        
                    let alert = UIAlertController(title: "Alert", message: "Please enter old password", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
                }
       
        if !isValidPassword(testStr: oldPassword_Txt.text!) {
             
             let alert = UIAlertController(title: "Alert", message: "Please enter atleast 6 digit (Alpha + Numeric)", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
             present(alert, animated: true,completion: {return})
            
        }
        if newPassword_txt.text!.isEmpty  {
        
                    let alert = UIAlertController(title: "Alert", message: "Please enter new password", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
                }
       
        if !isValidPassword(testStr: newPassword_txt.text!) {
             
             let alert = UIAlertController(title: "Alert", message: "Please enter atleast 6 digit (Alpha + Numeric)", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
             present(alert, animated: true,completion: {return})
            
        }
        
        if confirm_NewPasswordTxt.text!.isEmpty  {
        
                    let alert = UIAlertController(title: "Alert", message: "Please enter confirm new password", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
                }
       
        if !isValidPassword(testStr: confirm_NewPasswordTxt.text!) {
             
             let alert = UIAlertController(title: "Alert", message: "Password does not match, please try again", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
             present(alert, animated: true,completion: {return})
            
        }
        
        if (newPassword_txt.text == confirm_NewPasswordTxt.text) && (newPassword_txt.text != "" || confirm_NewPasswordTxt.text != "")  {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(vc, animated: true)
        
//        let vc: ViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//
////            self.tabBarController?.hidesBottomBarWhenPushed = true
//
//            self.navigationController?.pushViewController(vc, animated: true)
//        self.addChild(vc)
//
//        vc.view.frame = self.view.frame
//        self.view.addSubview(vc.view)
//        self.view.bringSubviewToFront(vc.view)
//        vc.didMove(toParent: self)
        }
        
        else if (newPassword_txt.text != confirm_NewPasswordTxt.text) {
            let alert = UIAlertController(title: "Alert", message: "Password does not match, please try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
           
            present(alert, animated: true,completion: {return})
            
        }
    
            }
       
    
    @IBAction func back_Btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)    }
}
