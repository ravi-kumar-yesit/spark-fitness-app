//
//  DownloadsTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 27/12/21.
//

import UIKit

class DownloadsTableViewCell: UITableViewCell {
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var downloadsImg: UIImageView!
    
    @IBOutlet weak var MuscleGroupLbl: UILabel!
    @IBOutlet weak var EquipmentLbl: UILabel!
    @IBOutlet weak var IntensityLbl: UILabel!
    @IBOutlet weak var RatingLbl: UILabel!
    @IBOutlet weak var TotalTimeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
    @IBAction func DeleteBtn_Action(_ sender: Any) {
        
    }
}
