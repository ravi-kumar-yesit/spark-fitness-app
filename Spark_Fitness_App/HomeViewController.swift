//
//  HomeViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 16/12/21.
//

import UIKit
import SideMenu

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate, UITableViewDataSource {
    var counter = 0
    @IBOutlet weak var btnChallengeWon: UIButton!
    @IBOutlet weak var btnCompleteChallenge: UIButton!
    @IBOutlet weak var btnAllChallenge: UIButton!
    @IBOutlet weak var whatsNewBtnIcon: UIButton!
    @IBOutlet weak var challengeTblView: UITableView!
    // Challenge details
    var challengeArray = ["Fitness","Dummy text","Dummy text","Fitness","Dummy text","Dummy text"]
    var challengetextArray = ["Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor."]
    var imgArray = ["SparkImg","SparkImg","SparkImg","SparkImg","SparkImg","SparkImg"]
    
    @IBOutlet weak var challengeBtnIcon: UIButton!
    @IBOutlet weak var view_challengeDetails: UIView!
    @IBOutlet weak var view_challengeType: UIView!
    @IBOutlet weak var view_WhatsNew: UIView!
    @IBOutlet weak var challengeBtn: UIButton!
    @IBOutlet weak var whatsNewBtn: UIButton!
    @IBOutlet var sideview: UIView!
    
    @IBOutlet weak var goalTableView: UITableView!
    
    let transition = slideTransition()
    var tapGesture = UITapGestureRecognizer()
    var timer = Timer()
    var currentcellIndex = 0
    var isSideViewOpen: Bool = false
    // Goal Details
    var goalArray = ["Goal -","Goal -","Goal -"]
    var goalDescriptionArray = ["1","Dummy text","Dummy text"]
    var imageArray = ["img1","img2","img3","img4","img5","img6","img7","img8","img9"]
    // var isSideViewOpen: Bool = false
    @IBOutlet weak var homeCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_WhatsNew.isHidden = false
        view_challengeType.isHidden = true
        view_challengeDetails.isHidden = true
        
        self.navigationController?.isNavigationBarHidden = true
        self.goalTableView.dataSource = self
        self.goalTableView.delegate = self
        challengeTblView.delegate = self
        challengeTblView.dataSource = self
        //        sideview.isHidden = true
        //        sidebartableview.backgroundColor = UIColor.groupTableViewBackground
        //        isSideViewOpen = false
        //
        //
        self.homeCollectionView.delegate = self
        self.homeCollectionView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeDrawer), name: Notification.Name.init(rawValue: "drawer"), object: nil)
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.closeDrawer), name: Notification.Name.init(rawValue: "drawer"), object: nil)
        counter = 0
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self)
//    }
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return challengeArray.count
    //    }
    //
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let  cell = challengeTblView.dequeueReusableCell(withIdentifier: "ChallengesTableViewCell", for: indexPath) as! ChallengesTableViewCell
    //        cell.challengeTitleLbl.text = challengeArray[indexPath.row]
    //
    //        cell.imgChallenge.image = UIImage(named: imgArray[indexPath.row])
    //        cell.challengedescriptionLbl.text = challengetextArray[indexPath.row]
    //        return cell
    //
    //    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell1 = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
        cell1.homeImg.image = UIImage(named: imageArray[indexPath.row])
        return cell1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = ((collectionView.frame.size.width))/3
        return CGSize(width: size-5, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 5.0, bottom: 10.0, right: 5.0)//here your custom value for spacing
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == goalTableView {
            return 35
        }
        else {
            return 130
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == goalTableView {
            return goalArray.count
        }
        else {
            return challengeArray.count
        }
        
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == goalTableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GoalTableViewCell
            cell.goalTitleLbl.text = goalArray[indexPath.row]
            cell.goalDescriptionLbl.text = goalDescriptionArray[indexPath.row]
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChallengesTableViewCell
            cell.challengeTitleLbl.text = challengeArray[indexPath.row]
            cell.challengedescriptionLbl.text = challengetextArray[indexPath.row]
            cell.view_challengeData.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            //cell.imgChallenge.image = UIImage(named: imageArray[indexPath.row])
            if indexPath.row == 0 {
                cell.view_challengeData.backgroundColor = hexStringToUIColor(hex: "353849")}
            if indexPath.row == 2 {
                cell.view_challengeData.backgroundColor = hexStringToUIColor(hex: "353849")}
            
            return cell
        }
        
    }
    @IBAction func challengeBtn_Action(_ sender: Any) {
        
        challengeBtnIcon.tintColor = .white
        whatsNewBtnIcon.tintColor = .orange
        challengeBtnIcon.tintColor = .orange
        whatsNewBtn.tintColor = .white
        
        view_WhatsNew.isHidden = true
        view_challengeType.isHidden = false
        view_challengeDetails.isHidden = false
        whatsNewBtn.setTitleColor(.white, for:.normal)
        
       challengeBtnIcon.setImage(UIImage(named: "challengeorangeicon"), for: .normal)
        whatsNewBtnIcon.setImage(UIImage(named: "whatsnewwhiteicon"), for: .normal)
        
      //  whatsnewwhiteicon
       // challengeorangeicon
        
        // whatsNewBtnIcon.setTitleColor(.white, for: .normal)
        challengeBtn.setTitleColor(hexStringToUIColor(hex: "FF9505"), for: .normal)
        //        challengeBtnIcon.setTitleColor(.orange, for: .normal)
    }
    @IBAction func challengeBtnIcon_Action(_ sender: Any) {
        //        if challengeBtn.isSelected == true {
        //            challengeBtnIcon.tintColor = .orange
        //
        //            challengeBtnIcon.isSelected = true
        //            whatsNewBtnIcon.isSelected = false
        //
        //
        //        }
        
        
        
    }
    
    @IBAction func btnallChallengeAction(_ sender: Any) {
        
        btnAllChallenge.backgroundColor = hexStringToUIColor(hex: "FF9505")
        btnChallengeWon.backgroundColor = hexStringToUIColor(hex: "151720")
        btnCompleteChallenge.backgroundColor = hexStringToUIColor(hex: "151720")
    }
    
    @IBAction func btnChallengeWonAction(_ sender: Any) {
        
        btnChallengeWon.backgroundColor = hexStringToUIColor(hex: "FF9505")
        btnCompleteChallenge.backgroundColor = hexStringToUIColor(hex: "151720")
        btnAllChallenge.backgroundColor = hexStringToUIColor(hex: "151720")
    }
    @IBAction func btnCompleteChallengeAction(_ sender: Any) {
        btnAllChallenge.backgroundColor = hexStringToUIColor(hex: "151720")
        btnChallengeWon.backgroundColor = hexStringToUIColor(hex: "151720")
        btnCompleteChallenge.backgroundColor = hexStringToUIColor(hex: "FF9505")
    }
    
    
    @IBAction func whatsNewBtn_Action(_ sender: Any) {
        
        challengeBtnIcon.tintColor = .white
        challengeBtn.tintColor = .white
        whatsNewBtnIcon.tintColor = .orange
        whatsNewBtn.tintColor = .orange
        
        view_WhatsNew.isHidden = false
        view_challengeType.isHidden = true
        view_challengeDetails.isHidden = true
        whatsNewBtn.setTitleColor(hexStringToUIColor(hex: "FF9505"), for:.normal)
     
      //  whatsneworange
        // challengeWhiteicon
        
        
        
        challengeBtnIcon.setImage(UIImage(named: "challengeWhiteicon"), for: .normal)
        whatsNewBtnIcon.setImage(UIImage(named: "whatsneworange"), for: .normal)
        
        //   whatsNewBtnIcon.setTitleColor(.orange, for: .normal)
        challengeBtn.setTitleColor(.white, for: .normal)
        //challengeBtnIcon.setTitleColor(.white, for: .normal)
        
    }
    
    
    @objc func closeDrawer(notification: Notification){
        
        let object = notification.object as? [String:Any]
        
        switch (object!["object"] as! Int){
        
        case 0:
            
            self.tabBarController?.hidesBottomBarWhenPushed = false
            self.hidesBottomBarWhenPushed = false
            self.tabBarController?.tabBar.isHidden = false
            
            //   self.tabBarController?.tabBar.ishidden = false
            // viewController.hidesBottomBarWhenPushed = false
            
       // tabBarController?.tabBar.tintColor = UIColor.orange
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
            //self.navigationController?.popViewController(animated: false)
           
        //            self.navigationController?.popToRootViewController(animated: true)
        //            break
        
        
        //            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        //            self.navigationController?.pushViewController(viewController, animated: true)
      
        
        //           self.navigationController?.pushViewController(viewController, animated: true)
        //             let viewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        //            self.tabBarController?.hidesBottomBarWhenPushed = false
        //            self.hidesBottomBarWhenPushed = false
        //            self.tabBarController?.tabBar.isHidden = false
        //         //   self.tabBarController?.tabBar.ishidden = false
        //            viewController.hidesBottomBarWhenPushed = false
        //            self.navigationController?.pushViewController(viewController, animated: true)
        
        case 1:
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
            
            self.tabBarController?.hidesBottomBarWhenPushed = false
            self.hidesBottomBarWhenPushed = false
            self.tabBarController?.tabBar.isHidden = false
            //   self.tabBarController?.tabBar.ishidden = false
            viewController.hidesBottomBarWhenPushed = false
           // UITabBar.appearance().tintColor = UIColor.white
           // tabBarController?.tabBar.tintColor = UIColor.white
            self.navigationController?.pushViewController(viewController, animated: true)
            
        case 2:
            self.tabBarController?.hidesBottomBarWhenPushed = false
            self.hidesBottomBarWhenPushed = false
            self.tabBarController?.tabBar.isHidden = false
            self.tabBarController?.selectedIndex = 1
            self.navigationController?.popToRootViewController(animated: true)
        //            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "FitnessViewController") as! FitnessViewController
        //
        //            self.tabBarController?.hidesBottomBarWhenPushed = false
        //            self.hidesBottomBarWhenPushed = false
        //            self.tabBarController?.tabBar.isHidden = false
        //           // self.tabBarController?.tabBar.ishidden = false
        //            viewController.hidesBottomBarWhenPushed = false
        
        //            self.navigationController?.pushViewController(viewController, animated: true)
        case 3:
            self.tabBarController?.selectedIndex = 2
            
            self.tabBarController?.hidesBottomBarWhenPushed = false
            self.hidesBottomBarWhenPushed = false
            self.tabBarController?.tabBar.isHidden = false
//            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
//            self.tabBarController?.hidesBottomBarWhenPushed = false
//            self.hidesBottomBarWhenPushed = false
//            self.tabBarController?.tabBar.isHidden = false
//            // self.tabBarController?.tabBar.ishidden = false
//            viewController.hidesBottomBarWhenPushed = false
//            self.navigationController?.pushViewController(viewController, animated: true)
        case 4:
            if counter == 0 {
                //self.tabBarController?.selectedIndex = 4
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController 
            self.tabBarController?.hidesBottomBarWhenPushed = true
            self.hidesBottomBarWhenPushed = true
            self.tabBarController?.tabBar.isHidden = true
            // self.tabBarController?.tabBar.ishidden = false
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
            } else {
            counter += 1
            }
        case 5:
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "TermConditionViewController") as! TermConditionViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        case 6:
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            
            self.tabBarController?.hidesBottomBarWhenPushed = false
            self.hidesBottomBarWhenPushed = false
            self.tabBarController?.tabBar.isHidden = false
            // self.tabBarController?.tabBar.ishidden = false
            viewController.hidesBottomBarWhenPushed = false
            
            self.navigationController?.pushViewController(viewController, animated: true)
        default:
            print("default")
        }
        
    }
    @IBAction func savedWorkOutBtn_Action(_ sender: Any) {
        
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DownloadsViewController") as! DownloadsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    //    @IBAction func challengesBtn_Action(_ sender: Any) {
    //
    //    }
    
    @IBAction func myGroupBtn(_ sender: UIButton) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "GroupsViewController") as! GroupsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func SidemenuBtn(_ sender: Any) {
        
        //        guard let SideBarViewController = storyboard?.instantiateViewController(withIdentifier: "SideBarViewController") else {return}
        //        SideBarViewController.modalPresentationStyle = .overCurrentContext
        //        SideBarViewController.transitioningDelegate = self
        //               present(SideBarViewController, animated: true)
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "SideBarViewController") as! SideBarViewController
        let leftMenuNavigationController = SideMenuNavigationController(rootViewController: secondViewController)
        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationController
        
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
        leftMenuNavigationController.presentationStyle = .menuSlideIn
        leftMenuNavigationController.menuWidth = self.view.frame.size.width-20
        self.present(leftMenuNavigationController, animated: true, completion: nil)
    }}


extension HomeViewController: UIViewControllerTransitioningDelegate{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = true
        return transition
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = false
        return transition
    }
    
    
}
