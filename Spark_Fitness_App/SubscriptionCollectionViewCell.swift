//
//  SubscriptionCollectionViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 28/12/21.
//

import UIKit

class SubscriptionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var plan1: UILabel!
    
    @IBOutlet weak var plan4Lbl: UILabel!
    @IBOutlet weak var plan3Lbl: UILabel!
    @IBOutlet weak var plan2Lbl: UILabel!
}
