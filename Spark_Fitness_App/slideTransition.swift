//
//  slideTransition.swift
//  signup
//
//  Created by Admin on 06/09/21.
//

import UIKit

class slideTransition: NSObject, UIViewControllerAnimatedTransitioning {
   
    var isPresenting = false
    let dimmingView = UIView()
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toViewController = transitionContext.viewController(forKey: .to),
        let fromViewController = transitionContext.viewController(forKey: .from) else {return}
        
        let containerview = transitionContext.containerView
        
        let finalWidth = toViewController.view.bounds.width*0.8
        let finalHeigt = toViewController.view.bounds.height
        
        // gesture
        
        if isPresenting{
            // Add dimming.view
            dimmingView.backgroundColor = .black
            dimmingView.alpha = 0.0
            containerview.addSubview(dimmingView)
            dimmingView.frame = containerview.bounds
            
            // Add menu controller to container
            containerview.addSubview(toViewController.view)
            
            //Init fram off the screen
            toViewController.view.frame = CGRect(x: -finalWidth, y: 0, width: finalWidth, height: finalHeigt)
        }
        
        
        //Animation on screen
        let transform = {
            self.dimmingView.alpha = 0.5
            toViewController.view.transform = CGAffineTransform(translationX: finalWidth, y: 0)
        }
        
        //Animation back off screen
        let identity = {
            self.dimmingView.alpha = 0.0
            fromViewController.view.transform = .identity
        }
        
        // Animation of the transition
        let duration = transitionDuration(using: transitionContext)
        let isCancelled = transitionContext.transitionWasCancelled
        UIView.animate(withDuration: duration, animations: {
                        self.isPresenting ? transform() : identity()
        }) { (_) in
            transitionContext.completeTransition(!isCancelled)
    }
        
        
        
    }

}
