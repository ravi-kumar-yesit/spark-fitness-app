//
//  CalendarViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 17/12/21.
//

import UIKit
import FSCalendar

class CalendarViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource,FSCalendarDelegateAppearance {
    @IBOutlet weak var view_Stack: UIStackView!
    var formatter = DateFormatter()
   // var datesWithEvent = ["15-01-22", "25-01-22", "30-01-22", "28-01-22"]
  
    
//    var datesWithMultipleEvents = ["05-10-22", "06-10-22", "03-10-22", "10-10-22"]
//
//    fileprivate lazy var dateFormatter2: DateFormatter = {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd MMMM yy"
//        return formatter
//    }()
    @IBOutlet weak var view_Descriptoncalender: UIView!
    @IBOutlet weak var addNewGoalBtn: UIButton!{didSet{
        addNewGoalBtn.layer.borderColor = UIColor.orange.cgColor
        addNewGoalBtn.layer.borderWidth = 2
    }}
    @IBOutlet weak var forward_button: UIButton!
    @IBOutlet weak var selectedDateLbl: UILabel!
    @IBOutlet var calender:FSCalendar!
    @IBOutlet weak var backward_button: UIButton!
    var delegate3 : AddNewGoalViewControllerCal? = nil
    
    override func viewDidLoad() {
        
//        view_Descriptoncalender.layer.borderColor = UIColor.red.cgColor
//        view_Descriptoncalender.layer.borderWidth = 2

     calender.delegate = self
 calender.dataSource = self
        calender.bringSubviewToFront(forward_button)
        calender.bringSubviewToFront(backward_button)
        // Do any additional setup after loading the view.
    }
    
//    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
//        let img = UIImage(named: "img3")
//        return img
//    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        let dateWithEvent = "15-01-2022"
            if dateWithEvent.contains("date") {
                   return dateWithEvent.contains("date") ? UIImage(named: "Star 4") : nil
        }
        return nil
        }
      

    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
       //formatter.dateFormat = "dd MMMM yy"
        formatter.dateFormat = "DD-MM-yyyy"
        guard let eventDate = formatter.date(from: "22-01-2022") else { return 0 }
        guard let eventDate1 = formatter.date(from: "27-01-2022") else { return 0 }
        guard let eventDate2 = formatter.date(from: "10-01-2022") else { return 0 }
        guard let eventDate3 = formatter.date(from: "01-01-2022") else { return 0 }
        
        if date.compare(eventDate) == .orderedSame || date.compare(eventDate1) == .orderedSame || date.compare(eventDate2) == .orderedSame || date.compare(eventDate3) == .orderedSame {
            return 3
        }
        return 0
        
        
//        let dateString = self.dateFormatter2.string(from: date)
//
//        if self.datesWithEvent.contains(dateString) {
//            return 1
//        }
//
//        if self.datesWithMultipleEvents.contains(dateString) {
//            return 3
//        }
//
//        return 0
  }
    
    var countMonth = 0
    
    @IBAction func action_forward(_ sender: Any) {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        countMonth += 1
        dateComponents.month = countMonth // For next button
//        dateComponents.month = -1 // For prev button
//        _calendar.date(byAdding: dateComponents, to: Date())
        let currentPage = _calendar.date(byAdding: dateComponents, to: Date())!
        self.calender.setCurrentPage(currentPage, animated: true)
        
    }
    
    @IBAction func action_backward(_ sender: Any) {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        countMonth -= 1
        dateComponents.month = countMonth // For next button
//        dateComponents.month = -1 // For prev button
//        _calendar.date(byAdding: dateComponents, to: Date())
        let currentPage = _calendar.date(byAdding: dateComponents, to: Date())!
        self.calender.setCurrentPage(currentPage, animated: true)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMMM yy"

//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.dateFormat = "MMM dd,yyyy"

        let dateStr = dateFormatterGet.string(from: date)
        selectedDateLbl.text = dateStr
//        let date: NSDate? = dateFormatterGet.dateFromString("2016-02-29 12:24:26")
//        print(dateFormatterPrint.stringFromDate(date!))
      //  selectedDateLbl.text = calendar.currentPage
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func AddNewGoalBtn(_ sender: Any) {
        
        let vc: AddNewGoalViewControllerCal = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGoalViewControllerCal") as! AddNewGoalViewControllerCal
            vc.delegate3 = self
            
        self.addChild(vc)
        
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        self.view.bringSubviewToFront(vc.view)
        vc.didMove(toParent: self)
        }
//        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "AddNewGoalViewControllerCal") as! AddNewGoalViewControllerCal
//        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
extension CalendarViewController: DelegateforAddNewGoalcal {
    func MethodforPop3() {
        
//        let vc: AddNewGoalViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGoalViewController") as! AddNewGoalViewController
    
//        vc.view.removeFromSuperview()
        
        if self.children.count > 0 {
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
                }
            }
    }
}
