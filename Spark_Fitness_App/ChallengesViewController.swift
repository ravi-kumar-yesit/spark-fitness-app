//
//  ChallengesViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 25/12/21.
//

import UIKit

class ChallengesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var view_challengeDeatails: UIView!
    @IBOutlet weak var view_differentChallenge: UIView!
    @IBOutlet weak var challengeBtn: UIButton!
    
    @IBOutlet weak var whatsNewBtn: UIButton!
    
    @IBOutlet weak var ChallengeTableView: UITableView!
    var challengeArray = ["Fitness","Dummy text","Dummy text","Fitness","Dummy text","Dummy text"]
    var challengetextArray = ["Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor.","Lorem ipsum dolor sit amet, consetu adipuk iscig elit. Aenean coodo ligula eget dolor."]
    var imgArray = ["SparkImg","SparkImg","SparkImg","SparkImg","SparkImg","SparkImg"]
    override func viewDidLoad() {
        super.viewDidLoad()

        view_differentChallenge.isHidden = true
        view_challengeDeatails.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return challengeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = ChallengeTableView.dequeueReusableCell(withIdentifier: "ChallengesTableViewCell", for: indexPath) as! ChallengesTableViewCell
        cell.challengeTitleLbl.text = challengeArray[indexPath.row]
        
        cell.imgChallenge.image = UIImage(named: imgArray[indexPath.row])
        cell.challengedescriptionLbl.text = challengetextArray[indexPath.row]
        return cell
      
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         if indexPath.row == 0 {
            let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChallengeDetailsViewController") as! ChallengeDetailsViewController
            
            self.navigationController?.pushViewController(secondViewController, animated: true)
         }
        if indexPath.row == 1 {
           let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChallengeDetailsViewController") as! ChallengeDetailsViewController
           
            self.navigationController?.pushViewController(secondViewController, animated: true)
            
        }
        if indexPath.row == 2 {
           let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChallengeDetailsViewController") as! ChallengeDetailsViewController
           
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        if indexPath.row == 3 {
           let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChallengeDetailsViewController") as! ChallengeDetailsViewController
           
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        if indexPath.row == 4 {
           let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChallengeDetailsViewController") as! ChallengeDetailsViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        if indexPath.row == 5 {
           let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChallengeDetailsViewController") as! ChallengeDetailsViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        
        
    }
    

}
