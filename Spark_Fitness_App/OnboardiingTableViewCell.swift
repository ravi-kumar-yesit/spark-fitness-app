//
//  OnboardiingTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 15/12/21.
//

import UIKit

class OnboardiingTableViewCell: UITableViewCell {
    @IBOutlet weak var save_Btn: UIButton!{didSet{
        save_Btn.layer.cornerRadius = save_Btn.layer.frame.height/2
        save_Btn.layer.borderWidth = 1
        save_Btn.layer.borderColor = UIColor.gray.cgColor
        
    }}
    @IBOutlet weak var view_ProfileBio: UIView!{didSet{
        save_Btn.layer.cornerRadius = 40
    }}
    @IBOutlet weak var view_Goals: UIView!{didSet{
        view_Goals.layer.cornerRadius = view_Goals.layer.frame.height/2
        view_Goals.layer.borderWidth = 1
        view_Goals.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_Weight: UIView!{didSet{
        view_Weight.layer.cornerRadius = view_Weight.layer.frame.height/2
        view_Weight.layer.borderWidth = 1
        view_Weight.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_Gender: UIView!{didSet{
        view_Gender.layer.cornerRadius = view_Gender.layer.frame.height/2
        view_Gender.layer.borderWidth = 1
        view_Gender.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_DOB: UIView!{didSet{
        view_DOB.layer.cornerRadius = view_DOB.layer.frame.height/2
        view_DOB.layer.borderWidth = 1
        view_DOB.layer.borderColor = UIColor.gray.cgColor
    }}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
