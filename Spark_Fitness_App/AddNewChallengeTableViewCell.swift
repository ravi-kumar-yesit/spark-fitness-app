//
//  AddNewChallengeTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 28/12/21.
//

import UIKit

class AddNewChallengeTableViewCell: UITableViewCell {

    @IBOutlet weak var selectBtn: UIButton!
    @IBOutlet weak var friendnameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func selectBtn(_ sender: Any) {
        if selectBtn.isSelected  == false {
            selectBtn.isSelected = true
                
            } else {
                selectBtn.isSelected = false
            }
    }
}
