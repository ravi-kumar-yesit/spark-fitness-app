//
//  GoalDetailsTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 24/12/21.
//

import UIKit

class GoalDetailsTableViewCell: UITableViewCell {
 
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var goalTitle: UILabel!
    @IBOutlet weak var goalCategoryLbl: UILabel!
    @IBOutlet weak var goaltypeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
