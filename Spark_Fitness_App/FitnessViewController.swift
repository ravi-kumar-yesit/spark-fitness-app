//
//  FitnessViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 17/12/21.
//

import UIKit
import SideMenu

class FitnessViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let transition = slideTransition()
    var tapGesture = UITapGestureRecognizer()
    var timer = Timer()
    var currentcellIndex = 0
    var isSideViewOpen: Bool = false
    
    @IBOutlet weak var fitnesstableview: UITableView!
    var imageArray = ["Rectangle 42","Rectangle 42-2","Rectangle 42-3","Rectangle 42-4"]
    
    
    var imageArray1 = ["Cardio","HIIT","Mobility","Strength"]
    
    var exerciseArray = ["Cardio","HIIT","Mobility","Strength"]
    
    var dataArray = [["name":"Cardio","image":"Rectangle 42","imageIcon":"Cardio"],["name":"HIIT","image":"Rectangle 42-2","imageIcon":"HIIT"],["name":"Mobility","image":"Rectangle 42-3","imageIcon":"Mobility"],["name":"Strength","image":"Rectangle 42","imageIcon":"Strength"]]
    
    @IBOutlet weak var tableViewFitness: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.closeDrawer), name: Notification.Name.init(rawValue: "drawer"), object: nil)
        
        self.navigationController?.isNavigationBarHidden = true
        self.fitnesstableview.delegate = self
        self.fitnesstableview.dataSource = self
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //    }
    @objc func closeDrawer(){
        print("Close drawer")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = dataArray[indexPath.row]
        
        let cell = fitnesstableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FitnessTableViewCell
        cell.imgFitness.image = UIImage(named: data["image"]!)
        cell.exerciselLbl.text = data["name"]
        
//        cell.star1?.addTarget(self, action: #selector(self.btnclick), for: .touchUpInside)
//        
       // cell.buttonname.tag = indexPath.section
        cell.view_TableData.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        
    
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        switch (indexPath.row) {
//        case 0:
        
        let data = dataArray[indexPath.row]
        
            let SVC = self.storyboard?.instantiateViewController(withIdentifier: "VedioDescriptionModeViewController") as! VedioDescriptionModeViewController
        SVC.dataDict = data
        self.navigationController?.pushViewController(SVC, animated: true)
    }
//            SVC.image1 = UIImage(named: imageArray[indexPath.row] )!
//            SVC.image2 = UIImage(named: imageArray[indexPath.row])!
//            
//            SVC.imageExerciseIconArray = UIImage(named: imageArray1[indexPath.row])!
//            SVC.imgDescriptionExerciseIcon1 = UIImage(named: imageArray1[indexPath.row])!
            
            
//        case 1:
//            let SVC = self.storyboard?.instantiateViewController(withIdentifier: "VedioDescriptionModeViewController") as! VedioDescriptionModeViewController
//            SVC.image1 = UIImage(named: imageArray[indexPath.row] )!
//            SVC.image2 = UIImage(named: imageArray[indexPath.row])!
//            SVC.imageExerciseIconArray = UIImage(named: imageArray1[indexPath.row])!
//            SVC.imgDescriptionExerciseIcon1 = UIImage(named: imageArray1[indexPath.row])!
//            self.navigationController?.pushViewController(SVC, animated: true)
//        case 2:
//            let SVC = self.storyboard?.instantiateViewController(withIdentifier: "VedioDescriptionModeViewController") as! VedioDescriptionModeViewController
//            SVC.image1 = UIImage(named: imageArray[indexPath.row] )!
//            SVC.image2 = UIImage(named: imageArray[indexPath.row])!
//            SVC.imageExerciseIconArray = UIImage(named: imageArray1[indexPath.row])!
//            SVC.imgDescriptionExerciseIcon1 = UIImage(named: imageArray1[indexPath.row])!
//            self.navigationController?.pushViewController(SVC, animated: true)        case 3:
//                let SVC = self.storyboard?.instantiateViewController(withIdentifier: "VedioDescriptionModeViewController") as! VedioDescriptionModeViewController
//                SVC.image1 = UIImage(named: imageArray[indexPath.row] )!
//                SVC.image2 = UIImage(named: imageArray[indexPath.row])!
//                SVC.imageExerciseIconArray = UIImage(named: imageArray1[indexPath.row])!
//                SVC.imgDescriptionExerciseIcon1 = UIImage(named: imageArray1[indexPath.row])!
//                self.navigationController?.pushViewController(SVC, animated: true)
//        default:
//            break
//
//        }
//    }
//    @objc func btnclick(){
//        let btn:UIButton
//        if btn.isSelected == true {
//            btn.setImage(Vector-31, for: UIControl.State.normal)
//        }
//        else {
//            btn.setImage(Vector-61, for: UIControl.State.normal)
//        }
//    }
    @IBAction func btnSideMenu(_ sender: Any) {
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "SideBarViewController") as! SideBarViewController
        let leftMenuNavigationController = SideMenuNavigationController(rootViewController: secondViewController)
        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationController
        
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
        leftMenuNavigationController.presentationStyle = .menuSlideIn
        leftMenuNavigationController.menuWidth = self.view.frame.size.width-20
        self.present(leftMenuNavigationController, animated: true, completion: nil)
        
       /*
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "SideBarViewController") as! SideBarViewController
        let leftMenuNavigationController = SideMenuNavigationController(rootViewController: secondViewController)
        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationController
        
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
        
        leftMenuNavigationController.menuWidth = self.view.frame.size.width-20
        self.present(leftMenuNavigationController, animated: true, completion: nil) */
        
    }
    
    @IBAction func btnFilter_Action(_ sender: Any) {
        
        let SVC = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(SVC, animated: true)
        
    }
}

extension FitnessViewController: UIViewControllerTransitioningDelegate{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = true
        return transition
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = false
        return transition
    }
    
    
}
