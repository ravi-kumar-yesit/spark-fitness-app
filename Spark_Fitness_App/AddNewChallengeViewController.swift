//
//  AddNewChallengeViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 25/12/21.
//

import UIKit
import DropDown

class AddNewChallengeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var ChallengeFriendTableView: UITableView!
    @IBOutlet weak var view_ChallangeAFriend: UIView!
    @IBOutlet weak var challengeAFriendTxt: UITextField!
   
    @IBOutlet weak var descriptionTxt: UITextField!
    @IBOutlet weak var titleTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Enter title",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        titleTxt.attributedPlaceholder = redPlaceholderText
    }}
    @IBOutlet weak var descriptiontxtview: UITextView!
    @IBOutlet weak var goalTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Select goal type",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        goalTxt.attributedPlaceholder = redPlaceholderText
    }}
    @IBOutlet weak var categoryTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Select category type",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        categoryTxt.attributedPlaceholder = redPlaceholderText
    }}
   
    var friendArray = ["Mary","Malachi","Margaret","Chris","John","Robert","Paul Anderson"]
    
    var filteredData = [String]()
    var searching = false
    let dropDown1 = DropDown()
    let dropDown2 = DropDown()
 
   
    @IBOutlet weak var searchBar: UISearchBar!
    var GoalArray = ["Daily Goals","Weekly Goals ","Monthly Goals"]
    var categoryArray = ["Strength","Strength","Strength"]
    override func viewDidLoad() {
        super.viewDidLoad()
        challengeAFriendTxt.addTarget(self, action: #selector(searchRecord), for: .editingChanged)
        ChallengeFriendTableView.delegate = self
        challengeAFriendTxt.delegate = self
        //searchbar.delegate = self
//        filteredData = friendArray
        
//        UIImage blank = [UIImage imageNamed:@"img4"];
//        [self.searchBar setImage:blank forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
//        self.searchBar.searchTextPositionAdjustment = UIOffsetMake(-19, 0)
//        searchbar.setImage(UIImage(named: "SearchIcon"), for: .search, state: .normal)
//        searchbar.setPositionAdjustment(UIOffset(horizontal: 180, vertical: 0), for: .search)
//        self.searchbar.searchTextPositionAdjustment = UIOffset(horizontal: -00, vertical: 0);
        dropDown1.anchorView = goalTxt
            dropDown1.dataSource = GoalArray
        dropDown1.direction = .any
        dropDown1.bottomOffset = CGPoint(x: 0, y:(dropDown1.anchorView?.plainView.bounds.height)!)
        dropDown1.topOffset = CGPoint(x: 0, y:-(dropDown1.anchorView?.plainView.bounds.height)!)
    
        dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            self.goalTxt.text = GoalArray[index]
            dropDown1.reloadAllComponents()
            
            dropDown2.anchorView = categoryTxt
                dropDown2.dataSource = categoryArray
            dropDown2.direction = .any
            dropDown2.bottomOffset = CGPoint(x: 0, y:(dropDown2.anchorView?.plainView.bounds.height)!)
            dropDown2.topOffset = CGPoint(x: 0, y:-(dropDown2.anchorView?.plainView.bounds.height)!)
        
            dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
              print("Selected item: \(item) at index: \(index)")
                self.categoryTxt.text = categoryArray[index]
                dropDown2.reloadAllComponents()
    }
            

        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    @objc func searchRecord (sender: UITextField) {
        self.filteredData.removeAll()
        let searchData:Int = challengeAFriendTxt.text!.count
        if searchData != 0 {
            searching = true
            for friend in friendArray {
                if let friendtoSearch = challengeAFriendTxt.text {
                    let range = friend.lowercased().range(of: friendtoSearch, options: .caseInsensitive, range: nil, locale: nil)
                    if range != nil {
                        
                        self.filteredData.append(friend)
                    }
                    
                }
              
            }
           
        }
        else {
            filteredData = friendArray
            searching = false
        }
        ChallengeFriendTableView.reloadData()
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //check search text & original text
        if( searching == true){
           return filteredData.count
        }else{
           return friendArray.count
        }
      }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if( searching == true) {
        let  cell = ChallengeFriendTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddNewChallengeTableViewCell
        cell.friendnameLbl.text = filteredData[indexPath.row]
        
            return cell}
        else{
                
                let  cell = ChallengeFriendTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddNewChallengeTableViewCell
                cell.friendnameLbl.text = friendArray[indexPath.row]
                    return cell
            }
    }

//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        // When there is no text, filteredData is the same as the original data
//        // When user has entered text into the search box
//        // Use the filter method to iterate over all items in the data array
//        // For each item, return true if the item should be included and false if the
//        // item should NOT be included
//        filteredData = searchText.isEmpty ? friendArray : friendArray.filter({(dataString: String) -> Bool in
//                // If dataItem matches the searchText, return true to include it
//                return dataString.range(of: searchText, options: .caseInsensitive) != nil
//            })
//        ChallengeFriendTableView.reloadData()
//    }
    @IBAction func GoalBtn_Action(_ sender: Any) {
        dropDown1.show()
    }
    @IBAction func categoryBtn_Action(_ sender: UIButton) {
        dropDown2.show()
    }
    @IBAction func BackBtn_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
