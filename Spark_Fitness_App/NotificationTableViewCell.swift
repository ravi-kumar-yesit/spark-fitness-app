//
//  NotificationTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 27/12/21.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
  
       
    @IBOutlet weak var view_TableData: UIView!{didSet {
        view_TableData.layer.cornerRadius = 5
        view_TableData.clipsToBounds = true
    }}
    
    @IBOutlet weak var descriptionLbl: UILabel!
     @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        view_TableData.layer.cornerRadius = 5
        view_TableData.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
