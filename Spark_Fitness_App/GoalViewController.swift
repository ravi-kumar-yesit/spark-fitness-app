//
//  GoalViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 23/12/21.
//

import UIKit
import DropDown

class GoalViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    @IBOutlet weak var BtnAdd: UIButton!{didSet{BtnAdd.layer.cornerRadius = 10
        BtnAdd.layer.borderWidth = 0.5
        BtnAdd.layer.borderColor = UIColor.lightGray.cgColor
    }}
    @IBOutlet weak var BtnCancel: UIButton!{didSet{BtnCancel.layer.cornerRadius = 10
        BtnCancel.layer.borderWidth =  0.5
        BtnCancel.layer.borderColor = UIColor.lightGray.cgColor
    }}
    @IBOutlet weak var view_GoalDescription: UIView!{didSet{
        view_GoalDescription.layer.cornerRadius = 10
        view_GoalDescription.layer.borderColor = UIColor.lightGray.cgColor
        view_GoalDescription.layer.borderWidth =  0.5
        view_GoalDescription.clipsToBounds = true
    }}
    @IBOutlet weak var view_TitleText: UIView!{didSet{
        view_TitleText.layer.cornerRadius = view_TitleText.layer.frame.height/2
        view_TitleText.layer.borderColor = UIColor.lightGray.cgColor
        view_TitleText.layer.borderWidth =  0.5
        view_TitleText.clipsToBounds = true
    }}
    @IBOutlet weak var view_Category: UIView!{didSet{
        view_Category.layer.cornerRadius = view_Category.layer.frame.height/2
        view_Category.layer.borderColor = UIColor.lightGray.cgColor
        view_Category.layer.borderWidth =  0.5
        view_Category.clipsToBounds = true
    }}
    @IBOutlet weak var View_GoalType: UIView!
    {didSet{
        View_GoalType.layer.cornerRadius = View_GoalType.layer.frame.height/2
        View_GoalType.layer.borderColor = UIColor.lightGray.cgColor
        View_GoalType.layer.borderWidth =  0.5
        View_GoalType.clipsToBounds = true
    }}
    
    @IBOutlet var popupView: UIView!
    
    @IBOutlet weak var selectGoalText: UITextField!{
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Select Goal Type",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
            
            selectGoalText.attributedPlaceholder = redPlaceholderText
        }
    }
    let dropDown1 = DropDown()
   
    var goalArray = ["Daily Goals","Weekly Goals","Monthly Goals"]
    
   
    @IBOutlet weak var BtnAddChallenges: UIButton!{didSet{
        BtnAddChallenges.layer.cornerRadius = BtnAddChallenges.layer.frame.height/2
        BtnAddChallenges.clipsToBounds = true
    }}
    
    @IBOutlet weak var BtnAddNew: UIButton!{didSet{
        BtnAddNew.layer.cornerRadius = BtnAddNew.layer.frame.height/2
        BtnAddNew.clipsToBounds = true
    }}
    var Goalname = ["Lorem ipsum elit.","Lorem ipsum elit.","Lorem ipsum elit.","Lorem ipsum elit."]
    var GoalBtn = ["Strength","Strength","Strength","Strength"]
    var GoalDescription = ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","Lorem ipsum dolor sit amet, consectetuer adipiscing elit.","Lorem ipsum dolor sit amet, consectetuer adipiscing elit."]

    @IBOutlet weak var GoalTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = true
//        popupView.frame = self.view .frame
//                self.view.addSubview(self.popupView)
//        popupView.isHidden = true
        
        
        dropDown1.anchorView = selectGoalText
            dropDown1.dataSource = goalArray
        dropDown1.direction = .any
        dropDown1.bottomOffset = CGPoint(x: 0, y:(dropDown1.anchorView?.plainView.bounds.height)!)
        dropDown1.topOffset = CGPoint(x: 0, y:-(dropDown1.anchorView?.plainView.bounds.height)!)
    
        dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            self.selectGoalText.text = goalArray[index]
            dropDown1.reloadAllComponents()
        // Do any additional setup after loading the view.
    }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
        

        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Goalname.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = GoalTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! YourGoalTableViewCell
        cell.Label1.text = Goalname[indexPath.row]
        cell.Label2.text = GoalBtn[indexPath.row]
        cell.Label3.text = GoalDescription[indexPath.row]
        cell.view_TableData.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        return cell
    }
    @IBAction func duratiionBtn(_ sender: Any) {
        dropDown1.show()
    }

    @IBAction func BtnAddNew(_ sender: UIButton) {
        
        
        let vc: AddNewGoalViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGoalViewController") as! AddNewGoalViewController
        vc.delegate = self
            self.addChild(vc)
            
            vc.view.frame = self.view.frame
            self.view.addSubview(vc.view)
            vc.didMove(toParent: self)
        
    }
  
    
    @IBAction func BtnAddChlg(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewChallengeViewController") as? AddNewChallengeViewController
        
        
        navigationController?.pushViewController(vc!, animated: true)
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
           let vc = storyboard?.instantiateViewController(withIdentifier: "GoalDetailsViewController") as? GoalDetailsViewController
//           vc?.image = UIImage(named: imgArray[indexPath.row] )!
//           vc?.name = NameArray[indexPath.row]
           navigationController?.pushViewController(vc!, animated: true)
    }
        else if indexPath.row == 1 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GoalDetailsViewController") as? GoalDetailsViewController
//            vc?.image = UIImage(named: imgArray[indexPath.row] )!
//            vc?.name = NameArray[indexPath.row]
            navigationController?.pushViewController(vc!, animated: true)
     }
        else if indexPath.row == 2 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GoalDetailsViewController") as? GoalDetailsViewController
//            vc?.image = UIImage(named: imgArray[indexPath.row] )!
//            vc?.name = NameArray[indexPath.row]
            navigationController?.pushViewController(vc!, animated: true)
     }
        else if indexPath.row == 3 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "GoalDetailsViewController") as? GoalDetailsViewController
//            vc?.image = UIImage(named: imgArray[indexPath.row] )!
//            vc?.name = NameArray[indexPath.row]
            navigationController?.pushViewController(vc!, animated: true)
     }
    }
    
    @IBAction func backBtn_Action(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as? SettingViewController
//
     //   navigationController?.pushViewController(vc!, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
}

extension GoalViewController: DelegateforAddNewGoal {
    func MethodforPop() {
        
//        let vc: AddNewGoalViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGoalViewController") as! AddNewGoalViewController
    
//        vc.view.removeFromSuperview()
        
        if self.children.count > 0 {
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
                }
            }
            
//            vc.view.frame = self.view.frame
//            self.view.addSubview(vc.view)
//            vc.didMove(toParent: self)
    }
    
}
