//
//  AnotherUserProfileViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 22/12/21.
//

import UIKit

class AnotherUserProfileViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var view_Vedio: UIView!
    
    @IBOutlet weak var BtnVedios: UIButton!
    
    @IBOutlet weak var BtnImages: UIButton!
    @IBOutlet weak var view_Image: UIView!
    //    @IBOutlet weak var imgImage: UIImageView!
    //    @IBOutlet weak var vedioImage: UIImageView!
    @IBOutlet weak var btn_Follow: UIButton!{didSet{
        btn_Follow.layer.cornerRadius = 10
    }}
    @IBOutlet weak var AnotherUserProfileTableview: UITableView!
    
    @IBOutlet weak var BtnImage: UIButton!
    @IBOutlet weak var btnVedio: UIButton!
    @IBOutlet weak var profielNameLbl: UILabel!
    
    var profileDescriptionArray = ["Lorem ipsum dolor sit Aadipisci elit Aenean commodo Ligula eget dolor Aenean massa"]
    var imageArray = ["img1","img2","img3","img4","img5","img6","img7","img8","img9"]
    @IBOutlet weak var AnotherUserProfileCollectionView1: UICollectionView!
    
    @IBOutlet weak var AnotherUserProfileCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //imgVedioPlay.isHidden = true
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    @IBAction func BackBtn_Action(_ sender: Any) {
        
        self.tabBarController?.hidesBottomBarWhenPushed = false
        self.hidesBottomBarWhenPushed = false
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == AnotherUserProfileCollectionView1)  {      return imageArray.count }
        else {
            return imageArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == AnotherUserProfileCollectionView) {
            
            let cell1 = AnotherUserProfileCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AnotherUserProfileCollectionViewCell
            cell1.img_Profile.image = UIImage(named: imageArray[indexPath.row])
            return cell1
        }
        else  {
            let cell1 = AnotherUserProfileCollectionView1.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! AnotherUserImageCollectionViewCell
            cell1.img_Profile1.image = UIImage(named: imageArray[indexPath.row])
            return cell1
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = ((collectionView.frame.size.width))/3
        return CGSize(width: size-5, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 5.0, bottom: 10.0, right: 5.0)//here your custom value for spacing
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileDescriptionArray.count
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    @IBAction func BtnVedioAction(_ sender: Any) {
        BtnImages.setImage(UIImage(named: "ImgImage"), for: .normal)
        BtnVedios.setImage(UIImage(named: "Group272"), for: .normal)
        
        
        view_Image.isHidden = true
        view_Vedio.isHidden = false
        
        btnVedio.setTitleColor(hexStringToUIColor(hex: "FF9505"), for: .normal)
        BtnImage.setTitleColor(.white, for: .normal)
        // imgImage.tintColor = UIColor.red
    }
    @IBAction func BtnImageAction(_ sender: Any) {
        //imgVedioPlay.isHidden = true
        
        view_Image.isHidden = false
        view_Vedio.isHidden = true
        
        BtnImages.setImage(UIImage(named: "Vector-69"), for: .normal)
        BtnVedios.setImage(UIImage(named: "Vector-68"), for: .normal)
        
        BtnImage.setTitleColor(hexStringToUIColor(hex: "FF9505"), for: .normal)
        btnVedio.setTitleColor(.white, for: .normal)
        //vedioImage.tintColor = UIColor.black
        
    }
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    ////        let cell = AnotherUserProfileTableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ano
    //    }
}
