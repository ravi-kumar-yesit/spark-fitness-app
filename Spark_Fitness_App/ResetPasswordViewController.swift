//
//  ResetPasswordViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 15/12/21.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var ResetBtn: UIButton! {didSet{
        ResetBtn.layer.cornerRadius = ResetBtn.layer.frame.height/2
        ResetBtn.layer.borderWidth = 1
        ResetBtn.layer.borderColor = UIColor.gray.cgColor
      }
    }
    @IBOutlet weak var confirm_NewPasswordTxt: UITextField! {
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Confirm New Password",
         attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            confirm_NewPasswordTxt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var newPassword_txt: UITextField! {didSet{
            let redPlaceholderText = NSAttributedString(string: "New Password",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            newPassword_txt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var view_ConfirmNewPassword: UIView! {didSet{
        view_ConfirmNewPassword.layer.cornerRadius = view_ConfirmNewPassword.layer.frame.height/2
        view_ConfirmNewPassword.layer.borderWidth = 1
        view_ConfirmNewPassword.layer.borderColor = UIColor.gray.cgColor
      }
    }
    @IBOutlet weak var view_NewPassword: UIView!{didSet{
        view_NewPassword.layer.cornerRadius = view_NewPassword.layer.frame.height/2
        view_NewPassword.layer.borderWidth = 1
        view_NewPassword.layer.borderColor = UIColor.gray.cgColor
      }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
       "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}")
//        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
//       "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}")
        return passwordTest.evaluate(with: testStr)
    }

    @IBAction func resetBtn_Action(_ sender: Any) {
        
        if newPassword_txt.text!.isEmpty  {
        
                    let alert = UIAlertController(title: "Alert", message: "Please enter new password", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
                }
       
        if !isValidPassword(testStr: newPassword_txt.text!) {
             
             let alert = UIAlertController(title: "Alert", message: "Please enter atleast 6 digit (Alpha + Numeric)", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
             present(alert, animated: true,completion: {return})
            
        }
        
        if confirm_NewPasswordTxt.text!.isEmpty  {
        
                    let alert = UIAlertController(title: "Alert", message: "Please enter confirm new password", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
                }
       
        if !isValidPassword(testStr: confirm_NewPasswordTxt.text!) {
             
             let alert = UIAlertController(title: "Alert", message: "Please enter atleast 6 digit (Alpha + Numeric)", preferredStyle: .alert)
             alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
             present(alert, animated: true,completion: {return})
            
        }
        
        if (newPassword_txt.text != confirm_NewPasswordTxt.text) {
            let alert = UIAlertController(title: "Alert", message: "Password doesn't match,  please try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
           
            present(alert, animated: true,completion: {return})
        }
        
        
        if (newPassword_txt.text == confirm_NewPasswordTxt.text) && (newPassword_txt.text != "" || confirm_NewPasswordTxt.text != "") {
        
        let vc: PasswordChangedViewController = self.storyboard?.instantiateViewController(withIdentifier: "PasswordChangedViewController") as! PasswordChangedViewController
                          
        self.addChild(vc)
        
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        self.view.bringSubviewToFront(vc.view)
        vc.didMove(toParent: self)
        }
    }
    @IBAction func back_Btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
