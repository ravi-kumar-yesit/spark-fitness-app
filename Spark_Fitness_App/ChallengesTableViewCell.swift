//
//  ChallengesTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 25/12/21.
//

import UIKit

class ChallengesTableViewCell: UITableViewCell {

    @IBOutlet weak var view_challengeData: UIView!
    @IBOutlet weak var challengedescriptionLbl: UILabel!
    @IBOutlet weak var challengeTitleLbl: UILabel!
    @IBOutlet weak var imgChallenge: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
