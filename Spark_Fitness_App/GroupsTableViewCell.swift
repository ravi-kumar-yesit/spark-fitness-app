//
//  GroupsTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 27/12/21.
//

import UIKit

class GroupsTableViewCell: UITableViewCell {

    @IBOutlet weak var view_tableData: UIView!
    @IBOutlet weak var groupImg: UIImageView!
    @IBOutlet weak var groupDescription: UILabel!
    @IBOutlet weak var groupTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
