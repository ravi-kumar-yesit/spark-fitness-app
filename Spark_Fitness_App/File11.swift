//
//  File11.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 22/12/21.
//

//
//  TabBarIndicatorView.swift
//
//  Created by Egzon Pllana on 11/15/20.
//  Copyright © 2020 Native Coders. All rights reserved.
//
import UIKit

class TabBarIndicatorView: UIView {

    // MARK: - Properties
    weak var tabBarController: UITabBarController! // Avoid retain memory cycles
    var animationDuration: Double!
    var spacing: CGFloat!

    // MARK: - Required Init
    init(atTabBarController tabBarController: UITabBarController,
         withItemHeight itemHeight: CGFloat = 1.5,
         withTabColor tabColor: UIColor = .black,
         withSpacing spacing: CGFloat = 4,
         withAnimationDuration animationDuration: Double = 0.20) {

        guard let tabView = tabBarController.tabBar.items?[0].value(forKey: "view") as? UIView else {
            super.init(frame: CGRect.zero)
            return
        }

        self.tabBarController = tabBarController
        self.animationDuration = animationDuration
        self.spacing = spacing

        // Setup View Frame
        super.init(frame: CGRect(x: tabView.frame.minX+spacing/2,
                                 y: tabBarController.tabBar.frame.minY+0.1,
                                 width: tabView.frame.size.width-spacing,
                                 height: itemHeight))

        // SetupUI
        self.backgroundColor = tabColor
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: - Methods
    func selectIndex(_ index: Int) {
        UIView.animate(withDuration: animationDuration) { [self] in
            guard let tabView = tabBarController.tabBar.items?[index].value(forKey: "view") as? UIView else {
                return
            }
            self.frame = CGRect(x: tabView.frame.minX+spacing/2,
                                y: tabBarController.tabBar.frame.minY+0.1,
                                width: tabView.frame.size.width-spacing,
                                height: self.frame.size.height)
        }
    }
}
