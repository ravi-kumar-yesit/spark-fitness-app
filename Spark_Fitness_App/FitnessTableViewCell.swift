//
//  FitnessTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 17/12/21.
//

import UIKit

class FitnessTableViewCell: UITableViewCell {
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star1: UIButton!
    @IBOutlet weak var imgFitness: UIImageView!
    @IBOutlet weak var view_TableData: UIView!
    
    @IBOutlet weak var exerciselLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func star1action(_ sender: Any) {  if star1.isSelected == false {
        star1.isSelected = true
    } else {
        star1.isSelected = false
    }
        
    }
    
    @IBAction func star2Action(_ sender: Any) {
        if star2.isSelected == false {
            star2.isSelected = true
        } else {
            star2.isSelected = false
        }
        
    }
    @IBAction func star3Action(_ sender: Any) {
        if star3.isSelected == false {
            star3.isSelected = true
        } else {
            star3.isSelected = false
        }
    }
    @IBAction func star4Action(_ sender: Any) { if star4.isSelected == false {
        star4.isSelected = true
    } else {
        star4.isSelected = false
    }
        
    }
}
