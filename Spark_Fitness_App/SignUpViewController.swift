//
//  SignUpViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 09/12/21.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var signUp_Btn: UIButton! {didSet{
        signUp_Btn.layer.cornerRadius = signUp_Btn.layer.frame.height/2
        signUp_Btn.layer.borderWidth = 1/4
        signUp_Btn.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_CreatePassword: UIView!{didSet{
        view_CreatePassword.layer.cornerRadius = view_CreatePassword.layer.frame.height/2
        view_CreatePassword.layer.borderWidth = 1/4
        view_CreatePassword.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_EmailPhone: UIView!{didSet{
        view_EmailPhone.layer.cornerRadius = view_EmailPhone.layer.frame.height/2
        view_EmailPhone.layer.borderWidth = 1/4
        view_EmailPhone.layer.borderColor = UIColor.gray.cgColor
        
    }}
    @IBOutlet weak var view_Fullname: UIView!{didSet{
        view_Fullname.layer.cornerRadius = view_Fullname.layer.frame.height/2
        view_Fullname.layer.borderWidth = 1/4
        view_Fullname.layer.borderColor = UIColor.gray.cgColor
        
    }}
    @IBOutlet weak var password_Txt: UITextField!{
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Create Password",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            
            password_Txt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var emailPhone_Txt: UITextField!{
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Email/Phone",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            
            emailPhone_Txt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var fullName_Txt: UITextField!{
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Full Name",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            
            fullName_Txt.attributedPlaceholder = redPlaceholderText
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    func isValidEmail(yourEmail: String)-> Bool{
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: yourEmail)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
                                       "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}")
        //        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
        //       "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}")
        return passwordTest.evaluate(with: testStr)
    }
    
    func isValidPhone(phone: String) -> Bool {
        let phoneRegex = "^[0-9+]{8,10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phone)
    }
    @IBAction func signUpBtn_Action(_ sender: Any) {
        
        if fullName_Txt.text!.isEmpty  {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter full name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
        }
        if !isValidEmail(yourEmail: emailPhone_Txt.text!) && !isValidPhone(phone: emailPhone_Txt.text!){
            let alert = UIAlertController(title: "Alert", message: "Please enter correct email/phone number", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true,completion: {return})
            
        }
        if !isValidPassword(testStr: password_Txt.text!) {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter atleast 6 digit (Alpha + Numeric)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            present(alert, animated: true,completion: {return})
            
        }
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "VerificaitonViewController") as! VerificaitonViewController
        
        self.navigationController!.pushViewController(secondViewController, animated: true)
        
    }
    
    @IBAction func SignUp_Btn_Action(_ sender: Any) {
        //        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "VerificaitonViewController") as! VerificaitonViewController
        //        self.navigationController!.pushViewController(secondViewController, animated: true)
        
        
        
    }
    @IBAction func LoginBtn_Action(_ sender: Any) {
        
//        UserDefaults.standard.set(true, forKey: "status")
//
//        AppDelegate.Switcher.updateRootVC()
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
}
