//
//  OnBoarding1ViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 15/12/21.
//

import UIKit
import DropDown

class OnBoarding1ViewController: UIViewController {
   
    @IBOutlet weak var Goals_Txt: UITextField!
    {didSet{
        Goals_Txt.attributedPlaceholder = NSAttributedString(string: "Goals", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
     }}
    @IBOutlet weak var Weight_Txt: UITextField!{didSet{
        Weight_Txt.attributedPlaceholder = NSAttributedString(string: "Weight", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
     }}
    @IBOutlet weak var Gender_txt: UITextField!{didSet{
        Gender_txt.attributedPlaceholder = NSAttributedString(string: "Gender", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
     }}
    @IBOutlet weak var DOB_Txt: UITextField!{didSet{
        DOB_Txt.attributedPlaceholder = NSAttributedString(string: "Date of Birth ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
     }}
    @IBOutlet weak var save_Btn: UIButton!{didSet{
        save_Btn.layer.cornerRadius = save_Btn.frame.size.height/2
        save_Btn.clipsToBounds = true
//        save_Btn.layer.borderWidth = 1
//        save_Btn.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_ProfileBio: UIView!{didSet{
        view_ProfileBio.layer.cornerRadius = 15
        
    }}
    @IBOutlet weak var view_Goals: UIView!{didSet{
        view_Goals.layer.cornerRadius = view_Goals.layer.frame.height/2
        view_Goals.layer.borderWidth = 1
        view_Goals.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_Weight: UIView!{didSet{
        view_Weight.layer.cornerRadius = view_Weight.layer.frame.height/2
        view_Weight.layer.borderWidth = 1
        view_Weight.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var test_button: UIButton!{
        didSet{
            test_button.layer.cornerRadius = test_button.frame.size.height/2
            test_button.clipsToBounds = true
        }
    }
    @IBOutlet weak var view_Gender: UIView!{didSet{
        view_Gender.layer.cornerRadius = view_Gender.layer.frame.height/2
        view_Gender.layer.borderWidth = 1
        view_Gender.layer.borderColor = UIColor.gray.cgColor
    }}
    @IBOutlet weak var view_DOB: UIView!{didSet{
        view_DOB.layer.cornerRadius = view_DOB.layer.frame.height/2
        view_DOB.layer.borderWidth = 1
        view_DOB.layer.borderColor = UIColor.gray.cgColor
    }}
    let dropDown1 = DropDown()
    var GenderArray = ["Male", "Female"]
    var GoalArray = ["Daily",
                     "Weekly",
                     "Monthly"]
    let dropDown2 = DropDown()
    var datePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.DOB_Txt.setInputViewDatePicker(target: self, selector: #selector(tapDone))
       
        // The view to which the drop down will appear on
        dropDown1.anchorView = Gender_txt
        dropDown2.anchorView = Goals_Txt
       

        // The list of items to display. Can be changed dynamically
        dropDown1.dataSource = GenderArray
        dropDown1.direction = .any
        dropDown1.bottomOffset = CGPoint(x: 0, y:(dropDown1.anchorView?.plainView.bounds.height)!)
        dropDown1.topOffset = CGPoint(x: 0, y:-(dropDown1.anchorView?.plainView.bounds.height)!)
    
        dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            self.Gender_txt.text = GenderArray[index]
            dropDown1.reloadAllComponents()

            
            // The list of items to display. Can be changed dynamically
            dropDown2.dataSource = GoalArray
            dropDown2.direction = .any
            dropDown2.bottomOffset = CGPoint(x: 0, y:(dropDown2.anchorView?.plainView.bounds.height)!)
            dropDown2.topOffset = CGPoint(x: 0, y:-(dropDown2.anchorView?.plainView.bounds.height)!)
        
            dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
              print("Selected item: \(item) at index: \(index)")
                self.Goals_Txt.text = GoalArray[index]
                dropDown2.reloadAllComponents()
            }
        // Do any additional setup after loading the view.
    }
    
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func tapDone() {
    
            if let datePicker = self.DOB_Txt.inputView as? UIDatePicker { // 2-1
                let dateformatter = DateFormatter() // 2-2
                dateformatter.dateStyle = .medium // 2-3
               
                self.DOB_Txt.text = dateformatter.string(from: datePicker.date) //2-4
                let dateFinal = DOB_Txt.text
                print(DOB_Txt.text!)
                print(dateFinal ?? 0)
                let calendar = Calendar.current
                var maxDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
                       maxDateComponent.day = 01
                        maxDateComponent.month = 01
                        maxDateComponent.year = 2022

                        let maxDate = calendar.date(from: maxDateComponent)
                print("max date : \(String(describing: maxDate))")
                
//                let formatter = DateFormatter()
//                formatter.dateFormat = "dd/MM/yyyy"
//                 let firstDate = formatter.date(from: "01/01/2022")
//                print(firstDate!)
//                let secondDate = formatter.date(from: "DOB_Txt.text")
//                print(secondDate!)
//                 if firstDate?.compare(secondDate!) == .orderedDescending {
//                     print("First Date is greater then second date")
//                 }
           
                
                
            }
            self.DOB_Txt.resignFirstResponder() // 2-5
        }

//    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    NSDate *currentDate = [NSDate date];
//    NSDateComponents *comps = [[NSDateComponents alloc] init];
//    [comps setMonth:1];
//    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
//
//    /* Optional for your case */
//    [comps setYear:-30];
//    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
//
//    [datePicker setMaximumDate:maxDate];
//    [datePicker setMinimumDate:minDate]
    
    @IBAction func dob_BtnAction(_ sender: Any) {
        
    }
    @IBAction func gender_BtnAction(_ sender: Any) {
        dropDown1.show()
    }
    @IBAction func goals_BtnAction(_ sender: Any) {
        dropDown2.show()
    }
    @IBAction func save_BtnAction(_ sender: Any) {
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    @IBAction func back_Btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
