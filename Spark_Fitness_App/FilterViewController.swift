//
//  FilterViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 17/12/21.
//

import UIKit
import DropDown

class FilterViewController: UIViewController {
    
    @IBOutlet weak var btnApplyFilter: UIButton!
    {didSet{
        btnApplyFilter.layer.cornerRadius = 10
    }}
    @IBOutlet weak var btnClearFilter: UIButton!
    {didSet{
        btnClearFilter.layer.cornerRadius = 10
    }}
    @IBOutlet weak var btnShoulder1: UIButton!
    @IBOutlet weak var btnUpperArm: UIButton!
    @IBOutlet weak var btnBack1: UIButton!
    @IBOutlet weak var btnShoulders: UIButton!
    @IBOutlet weak var btnWarmup: UIButton!
    
    @IBOutlet weak var btnLegs1: UIButton!
    @IBOutlet weak var BtnHips: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnChest: UIButton!
    @IBOutlet weak var btnArms: UIButton!
    @IBOutlet weak var btnUpper: UIButton!
    @IBOutlet weak var btnQuads: UIButton!
    @IBOutlet weak var btnHamstrings: UIButton!
    @IBOutlet weak var btnGlutches: UIButton!
    @IBOutlet weak var btnLower: UIButton!
    @IBOutlet weak var btnFullyBody: UIButton!
    @IBOutlet weak var btnBodyweight: UIButton!
    @IBOutlet weak var btnMusclecategoryshow: UIButton!
    @IBOutlet weak var savedWorkoutTxt: UITextField!
    @IBOutlet weak var ratingTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Select rating",
                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        ratingTxt.attributedPlaceholder = redPlaceholderText
        
    }}
    
    @IBOutlet weak var mobilityTxt: UITextField!{didSet{
    let redPlaceholderText = NSAttributedString(string: "Select muscle group",
                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    
    mobilityTxt.attributedPlaceholder = redPlaceholderText
    
}}

    
    @IBOutlet weak var muscleTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Select muscle group",
                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        muscleTxt.attributedPlaceholder = redPlaceholderText
        
    }}
    
    @IBOutlet weak var durationTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Select duration",
                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        durationTxt.attributedPlaceholder = redPlaceholderText
        
    }}
    @IBOutlet weak var view_Saved: UIView!
    {didSet{
        view_Saved.layer.cornerRadius = view_Saved.layer.frame.height/2
        view_Saved.layer.borderWidth = 1
        view_Saved.layer.borderColor = UIColor.gray.cgColor    }
    }
    @IBOutlet weak var view_Ratings: UIView! {didSet{
        view_Ratings.layer.cornerRadius = view_Ratings.layer.frame.height/2
        view_Ratings.layer.borderWidth = 1
        view_Ratings.layer.borderColor = UIColor.gray.cgColor    }
    }
    @IBOutlet weak var view_Mobility: UIView!{didSet{
        view_Mobility.layer.cornerRadius = view_Mobility.layer.frame.height/2
        view_Mobility.layer.borderWidth = 1
        view_Mobility.layer.borderColor = UIColor.gray.cgColor    }
    }
    @IBOutlet weak var view_MuscleGroup: UIView!{didSet{
        view_MuscleGroup.layer.cornerRadius = view_MuscleGroup.layer.frame.height/2
        view_MuscleGroup.layer.borderWidth = 1
        view_MuscleGroup.layer.borderColor = UIColor.gray.cgColor    }
    }
    @IBOutlet weak var view_MuscleCategory: UIView!
    @IBOutlet weak var view_Duration: UIView!{didSet{
        view_Duration.layer.cornerRadius = view_Duration.layer.frame.height/2
        view_Duration.layer.borderWidth = 1
        view_Duration.layer.borderColor = UIColor.gray.cgColor    }
    }
    @IBOutlet weak var view_MobilityPopUp: UIView!
    let dropDown1 = DropDown()
    let dropDown2 = DropDown()
    var DurationArray = ["5 minutes","10 minutes","20 minutes","30 minutes","45 minutes","30 minutes","1 hour"]
    var ratingArray = ["Highest to lowest",
    "Lowest to highest"]
    var inputString = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.hidesBottomBarWhenPushed = true
        self.hidesBottomBarWhenPushed = true
        self.tabBarController?.tabBar.isHidden = true
       // self.tabBarController?.tabBar.ishidden = false
       // FilterViewController.hidesBottomBarWhenPushed = true

        view_MobilityPopUp.isHidden = true
        view_MuscleCategory.isHidden = true
        
        dropDown1.anchorView = durationTxt
            dropDown1.dataSource = DurationArray
        dropDown1.direction = .any
        dropDown1.bottomOffset = CGPoint(x: 0, y:(dropDown1.anchorView?.plainView.bounds.height)!)
        dropDown1.topOffset = CGPoint(x: 0, y:-(dropDown1.anchorView?.plainView.bounds.height)!)
    
        dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            self.durationTxt.text = DurationArray[index]
            dropDown1.reloadAllComponents()
            
            dropDown2.anchorView = ratingTxt
                dropDown2.dataSource = ratingArray
            dropDown2.direction = .any
            dropDown2.bottomOffset = CGPoint(x: 0, y:(dropDown2.anchorView?.plainView.bounds.height)!)
            dropDown2.topOffset = CGPoint(x: 0, y:-(dropDown2.anchorView?.plainView.bounds.height)!)
        
            dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
              print("Selected item: \(item) at index: \(index)")
                self.ratingTxt.text = ratingArray[index]
                dropDown2.reloadAllComponents()
            }
        // Do any additional setup after loading the view.
    }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     @IBAction func BtnWarmUp(_ sender: Any) {
     }
     @IBAction func btnBack(_ sender: Any) {
     }
     */
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    @IBAction func btnUpperArms(_ sender: Any) {
        if btnUpperArm.isSelected  == false {
            btnUpperArm.isSelected = true
            mobilityTxt.text = "Arms Upper (chest/arms )"
            } else {
                btnUpperArm.isSelected = false
                mobilityTxt.text = ""
            }
        
    }
    @IBAction func btnBack1(_ sender: Any) {
        if btnBack1.isSelected  == false {
            btnBack1.isSelected = true
            mobilityTxt.text! += "Back "
                
            } else {
                btnBack1.isSelected = false
                mobilityTxt.text = ""
            }
    }
    
    @IBAction func btnLegs(_ sender: Any) {
        if btnLegs1.isSelected  == false {
            btnLegs1.isSelected = true
            mobilityTxt.text! += "legs (quad/hamstring)"
                
            } else {
                btnLegs1.isSelected = false
                mobilityTxt.text = ""
            }
        
    }
    @IBAction func btnShoulders(_ sender: Any) {
        if btnShoulders.isSelected  == false {
            btnShoulders.isSelected = true
            mobilityTxt.text! += "Shoulders"
                
            } else {
                btnShoulders.isSelected = false
                mobilityTxt.text = ""
            }
        
    }
    @IBAction func btnHips(_ sender: Any) {
        if BtnHips.isSelected  == false {
            BtnHips.isSelected = true
            mobilityTxt.text! += "Hips "
                
            } else {
                BtnHips.isSelected = false
                mobilityTxt.text = ""
            }
    }
    
    @IBAction func btnWarmup(_ sender: Any) {
        if btnWarmup.isSelected  == false {
            btnWarmup.isSelected = true
            mobilityTxt.text! += "Warm-up "
            } else {
                btnWarmup.isSelected = false
                mobilityTxt.text = ""
            }
        
    }
    @IBAction func btnMobilityPopUpAction(_ sender: Any) {
        if view_MobilityPopUp.isHidden == true {
            view_MobilityPopUp.isHidden = false
    }
        else if view_MobilityPopUp.isHidden == false {
            view_MobilityPopUp.isHidden = true
        }
    }
    @IBAction func btnMusclecategoryshow(_ sender: Any) {
        if view_MuscleCategory.isHidden == true {
            view_MuscleCategory.isHidden = false
    }
        else if view_MuscleCategory.isHidden == false {
            
            view_MuscleCategory.isHidden = true
        }
       
    }
    
    @IBAction func ApplyBtn_Action(_ sender: Any) {
        view_MuscleCategory.isHidden = true
    }
    @IBAction func clearBtn_Action(_ sender: Any) {
        muscleTxt.text = ""
        btnFullyBody.isSelected = false
        btnBodyweight.isSelected = false
    }
    
    @IBAction func Btnbodyweight(_ sender: UIButton) {
        if btnBodyweight.isSelected  == false {
            inputString.insert("Body Weight ", at: 0)
           // inputString[0] = "Body Weight"
            self.muscleTxt.text = inputString[0]
            btnBodyweight.isSelected = true
                
            } else {
                btnBodyweight.isSelected = false
                muscleTxt.text = ""
            }
    }
    @IBAction func btnFullbody(_ sender: Any) {
        if btnFullyBody.isSelected  == false {
            inputString.insert("Full Body ", at: 1)
           // inputString[1] = "Full Body"
            self.muscleTxt.text! += inputString[1]
            btnFullyBody.isSelected = true
                
            } else {
                btnFullyBody.isSelected = false
                muscleTxt.text = ""
            }
        
    }
    @IBAction func btnlower(_ sender: Any) {
        if btnLower.isSelected  == false {
            btnLower.isSelected = true
                
            } else {
                btnLower.isSelected = false
            }
    }
    @IBAction func btnGlutches(_ sender: Any) {
        if btnGlutches.isSelected  == false {
            btnGlutches.isSelected = true
                
            } else {
                btnGlutches.isSelected = false
            }
    }
    @IBAction func btnHamstring(_ sender: Any) {
        if btnHamstrings.isSelected  == false {
            btnHamstrings.isSelected = true
                
            } else {
                btnHamstrings.isSelected = false
            }
    }
    @IBAction func BtnBack(_ sender: Any) {
        if btnBack.isSelected  == false {
            btnBack.isSelected = true
                
            } else {
                btnBack.isSelected = false
            }
    }
    @IBAction func btnQuads(_ sender: Any) {
        if btnQuads.isSelected  == false {
            btnQuads.isSelected = true
                
            } else {
                btnQuads.isSelected = false
            }
    }
    @IBAction func btnUpper(_ sender: Any) {
        if btnUpper.isSelected  == false {
            btnUpper.isSelected = true
                
            } else {
                btnUpper.isSelected = false
            }
    }
    @IBAction func btnArms(_ sender: Any) {
        if btnArms.isSelected  == false {
            btnArms.isSelected = true
                
            } else {
                btnArms.isSelected = false
            }
    }
    @IBAction func BtnChest(_ sender: Any) {
        if btnChest.isSelected  == false {
            btnChest.isSelected = true
                
            } else {
                btnChest.isSelected = false
            }
    }
 
    @IBAction func btnShoulder(_ sender: Any) {
        if btnShoulder1.isSelected  == false {
            btnShoulder1.isSelected = true
                
            } else {
                btnShoulder1.isSelected = false
            }
    }
    @IBAction func back_Btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func duratiionBtn(_ sender: Any) {
        dropDown1.show()
    }
    @IBAction func ratingBtnAction(_ sender: Any) {
        dropDown2.show()
    }
    @IBAction func clearMobilityBtn_Action(_ sender: Any) {
        mobilityTxt.text = ""
        btnWarmup.isSelected = false
        BtnHips.isSelected = false
        btnShoulders.isSelected = false
        btnBack1.isSelected = false
        btnLegs1.isSelected = false
        btnUpperArm.isSelected = false
        
    }
    @IBAction func btnMobilityApply_Action(_ sender: Any) {
        view_MobilityPopUp.isHidden = true
    }
    
}
