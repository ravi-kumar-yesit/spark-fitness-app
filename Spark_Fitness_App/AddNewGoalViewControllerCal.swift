//
//  AddNewGoalViewControllerCal.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 19/01/22.
//

import UIKit
import DropDown

protocol DelegateforAddNewGoalcal {
    func MethodforPop3()
    
}
class AddNewGoalViewControllerCal : UIViewController {
   
    
    @IBOutlet weak var titleTxt: UITextField!
    {didSet{
        let redPlaceholderText = NSAttributedString(string: "Enter title",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        titleTxt.attributedPlaceholder = redPlaceholderText
    }
}
   
    @IBOutlet weak var descriptionTxtView: UITextView!
  
    @IBOutlet weak var categoryTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Select category type",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        categoryTxt.attributedPlaceholder = redPlaceholderText
    }
}
    @IBOutlet weak var goalTypeTxt: UITextField!{didSet{
        let redPlaceholderText = NSAttributedString(string: "Select goal type",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        goalTypeTxt.attributedPlaceholder = redPlaceholderText
    }
}
    @IBOutlet weak var view_AddNewGoal: UIView!
    @IBOutlet weak var BtnAdd: UIButton!{didSet{BtnAdd.layer.cornerRadius = 10
        BtnAdd.layer.borderWidth = 0.5
        BtnAdd.layer.borderColor = UIColor.lightGray.cgColor
    }}
    @IBOutlet weak var BtnCancel: UIButton!{didSet{BtnCancel.layer.cornerRadius = 10
        BtnCancel.layer.borderWidth =  0.5
        BtnCancel.layer.borderColor = UIColor.lightGray.cgColor
    }}
    @IBOutlet weak var view_GoalDescription: UIView!{didSet{
        view_GoalDescription.layer.cornerRadius = 10
        view_GoalDescription.layer.borderColor = UIColor.lightGray.cgColor
        view_GoalDescription.layer.borderWidth =  0.5
        view_GoalDescription.clipsToBounds = true
    }}
    @IBOutlet weak var view_TitleText: UIView!{didSet{
        view_TitleText.layer.cornerRadius = view_TitleText.layer.frame.height/2
        view_TitleText.layer.borderColor = UIColor.lightGray.cgColor
        view_TitleText.layer.borderWidth =  0.5
        view_TitleText.clipsToBounds = true
    }}
    @IBOutlet weak var view_Category: UIView!{didSet{
        view_Category.layer.cornerRadius = view_Category.layer.frame.height/2
        view_Category.layer.borderColor = UIColor.lightGray.cgColor
        view_Category.layer.borderWidth =  0.5
        view_Category.clipsToBounds = true
    }}
    @IBOutlet weak var View_GoalType: UIView!
    {didSet{
        View_GoalType.layer.cornerRadius = View_GoalType.layer.frame.height/2
        View_GoalType.layer.borderColor = UIColor.lightGray.cgColor
        View_GoalType.layer.borderWidth =  0.5
        View_GoalType.clipsToBounds = true
    }}
    let dropDown1 = DropDown()
    let dropDown2 = DropDown()
    let categoryArray = ["Strength","HIIT","Cardio","Mobility"]
    
       let goalTypeArray = ["Daily Goals","Weekly Goals","Monthly Goals"]
    
    var delegate3:DelegateforAddNewGoalcal? = nil
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dropDown1.anchorView = goalTypeTxt
                dropDown1.dataSource = goalTypeArray
        dropDown1.bottomOffset = CGPoint(x: 0, y:(dropDown1.anchorView?.plainView.bounds.height)!)
               dropDown1.topOffset = CGPoint(x: 0, y:-(dropDown1.anchorView?.plainView.bounds.height)!)
             
        dropDown1.direction = .any
        dropDown1.selectionAction = { [unowned self] (index: Int, item: String) in
             print("Selected item: \(item) at index: \(index)")
               self.goalTypeTxt.text = goalTypeArray[index]
               
              
        
        dropDown2.anchorView = categoryTxt
                 dropDown2.dataSource = categoryArray
        
        dropDown2.topOffset = CGPoint(x: 0, y:-(dropDown2.anchorView?.plainView.bounds.height)!)
        dropDown2.bottomOffset = CGPoint(x: 0, y:(dropDown2.anchorView?.plainView.bounds.height)!)
        dropDown2.direction = .any
            dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
              print("Selected item: \(item) at index: \(index)")
            self.categoryTxt.text = categoryArray[index]
    }
    
        }}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnGoalAction(_ sender: Any) {
        dropDown1.show()
    }
    
    @IBAction func categoryBtnAction(_ sender: Any) {
        dropDown2.show()
    }
    @IBAction func BtnCloseAddnewGoal(_ sender: Any) {
        
        self.delegate3?.MethodforPop3()
        
        
    //    self.navigationController?.popViewController(animated: true)
//        let SVC = self.storyboard?.instantiateViewController(withIdentifier: "GoalViewController") as! GoalViewController
//        self.navigationController?.pushViewController(SVC, animated: true)
    }
    @IBAction func CancelBtn_Action(_ sender: Any) {
        
        self.delegate3?.MethodforPop3()
        
     //        self.navigationController?.popViewController(animated: true)//
        
//        let SVC = self.storyboard?.instantiateViewController(withIdentifier: "GoalViewController") as! GoalViewController
//        self.navigationController?.pushViewController(SVC, animated: true)
    }
}
