//
//  EditProfileViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 19/01/22.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var txt_Weight: UITextField!{didSet{
        txt_Weight.attributedPlaceholder = NSAttributedString(string: "70", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }}
    @IBOutlet weak var txt_Phone: UITextField!{didSet{
        txt_Phone.attributedPlaceholder = NSAttributedString(string: "+12243359185", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }}
    @IBOutlet weak var txt_Email: UITextField!{didSet{
        txt_Email.attributedPlaceholder = NSAttributedString(string: "Michael12121@gmail.com", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }}
    @IBOutlet weak var txt_Name: UITextField!{didSet{
        txt_Name.attributedPlaceholder = NSAttributedString(string: "Michael", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }}
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func backBtn_Action(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SaveBtn_Action(_ sender: Any) {
        
        
        
        let alertController:UIAlertController = UIAlertController(title: "Alert", message: "Data saved successfully", preferredStyle: UIAlertController.Style.alert)
        
        let alertAction:UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:nil)
        let alertAction1:UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler:nil)
        
        // Add alertAction object to alertController.
        alertController.addAction(alertAction)
        alertController.addAction(alertAction1)
        
        // Popup the alert dialog.
        present(alertController, animated: true, completion: nil)
    }
   
}
