//
//  PreferrenceViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 28/12/21.
//

import UIKit

class PreferrenceViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var preferrenceTblView: UITableView!
    var imageArray = ["Rectangle 42","Rectangle 42-2","Rectangle 42-3","Rectangle 42-4"]
    var exerciseArray = ["Cardio","HIIT","Mobility","Strength"]
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = preferrenceTblView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PreferrenceTableViewCell
        cell.downloadsImg.image = UIImage(named: imageArray[indexPath.row])
        cell.TitleLbl.text = exerciseArray[indexPath.row]
        cell.view_PreferranceData.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
                exerciseArray.remove(at: indexPath.row)
            imageArray.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            preferrenceTblView.reloadData()
        }}
    @IBAction func backBtn_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ThreeDotBtn_Action(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: preferrenceTblView)
        guard let indexpath = preferrenceTblView.indexPathForRow(at: point) else {
            return}
        imageArray.remove(at: indexpath.row)
        exerciseArray.remove(at: indexpath.row)
        preferrenceTblView.deleteRows(at: [IndexPath(row: indexpath.row, section: 0)], with: .left)
        preferrenceTblView.endUpdates()
        
    }
    
}
