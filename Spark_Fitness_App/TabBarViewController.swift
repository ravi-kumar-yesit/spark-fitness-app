//
//  TabBarViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 16/12/21.
//

import UIKit

class TabBarViewController: TTabBarViewController {
    
    var indicatorView: TabBarIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
       // additionalSafeAreaInsets.bottom = 5
        tabBar.unselectedItemTintColor = .white
        
    
//        self.tabBarController?.hidesBottomBarWhenPushed = false
//        self.hidesBottomBarWhenPushed = false
//        self.tabBarController?.tabBar.isHidden = false
        
    
        // Customize Tab Bar
               // DispatchQueue.main.async {
                    
                  //  self.customizeTabBarUI()
              //  }

        // Do any additional setup after loading the view.
    }
   
//    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
//            let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
//            let rectLine = CGRect(x:0, y:size.height-lineSize.height,width: lineSize.width,height: lineSize.height)
//            UIGraphicsBeginImageContextWithOptions(size, false, 0)
//            UIColor.clear.setFill()
//            UIRectFill(rect)
//            color.setFill()
//            UIRectFill(rectLine)
//            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//            UIGraphicsEndImageContext()
//            return image
//        }
//    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//
//        if self.selectedIndex == 0 {
//            let rootView = self.viewControllers![self.selectedIndex] as! UINavigationController
//            rootView.popToRootViewController(animated: false)
//
//    }
//        if self.selectedIndex == 1 {
//            let rootView = self.viewControllers![self.selectedIndex] as! UINavigationController
//            rootView.popToRootViewController(animated: false)
//
//    }
//        if self.selectedIndex == 2 {
//            let rootView = self.viewControllers![self.selectedIndex] as! UINavigationController
//            rootView.popToRootViewController(animated: false)
//
//    }
//        if self.selectedIndex == 3 {
//            let rootView = self.viewControllers![self.selectedIndex] as! UINavigationController
//            rootView.popToRootViewController(animated: false)
//
//    }
//        if self.selectedIndex == 4 {
//            let rootView = self.viewControllers![self.selectedIndex] as! UINavigationController
//            rootView.popToRootViewController(animated: false)
//    }
//    }
    
}
    
extension TabBarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        
        // Update indicator view
        indicatorView.selectIndex(self.selectedIndex)
        
        
        if (self.tabBarController?.selectedIndex == 1){
            
            tabBarController.tabBar.tintColor = UIColor.white
            
        } else {
            tabBarController.tabBar.tintColor = UIColor.orange
        }
    }
}
    

