//
//  AppDelegate.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 09/12/21.
//

import UIKit
import IQKeyboardManager
import Cosmos
//import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

 //  var window : UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
       // Switcher.updateRootVC()
        
        // Override point for customization after application launch.
   IQKeyboardManager.shared().isEnabled = true
        //IQKeyboardManager.shared.enable = true
      //  Thread.sleep(forTimeInterval: 2.0)
        
       // (window?.rootViewController as! UITabBarController).tabBar.tintColor = UIColor.red
       UITabBar.appearance().selectionIndicatorImage = UIImage(named: "Line1")
        //let image = UIImageView(image: UIImage(named: imagesSymboleList[i]))
      
//        let userLoginStatus = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
//
//
//            if(userLoginStatus)
//            {
//                window = UIWindow()
//                let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
//
//                self.window!.rootViewController = centerVC
//                self.window!.makeKeyAndVisible()
//            }
        
  //  UITabBar.appearance().tintColor = UIColor.orange

             
        return true
    }
//    class Switcher {
//
//        static func updateRootVC(){
//
//            let status = UserDefaults.standard.bool(forKey: "status")
//            var rootVC : UIViewController?
//
//                print(status)
//
//
//            if(status == true){
//                rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
//            }else{
//                rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//            }
//
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.window?.rootViewController = rootVC
//
//        }
//
//    }
//
//
//
//
    

   
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

