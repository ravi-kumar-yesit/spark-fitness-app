//
//  K.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 19/01/22.
//

import UIKit

struct Segue {
    static let SignupScreen = "SignUpViewController"
    
    struct StoryboardID {
        static let main = "Main"
        static let SignUpViewController = "SignUpViewController"
    }
}
