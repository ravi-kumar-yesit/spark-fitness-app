//
//  PrivacyPolicyViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 21/12/21.
//

import UIKit
import SideMenu
import SideMenuSwift

class PrivacyPolicyViewController: UIViewController {
    
    let transition = slideTransition()
    var tapGesture = UITapGestureRecognizer()
    var timer = Timer()
    var currentcellIndex = 0
    var isSideViewOpen: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.closeDrawer), name: Notification.Name.init(rawValue: "drawer"), object: nil)
        //        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.hidesBottomBarWhenPushed = true
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    @objc func closeDrawer(){
        print("Close drawer")
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
        
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "SideBarViewController") as! SideBarViewController
        let leftMenuNavigationController = SideMenuNavigationController(rootViewController: secondViewController)
        SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationController
        
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
        
        leftMenuNavigationController.menuWidth = self.view.frame.size.width-20
        self.present(leftMenuNavigationController, animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension PrivacyPolicyViewController: UIViewControllerTransitioningDelegate{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = true
        return transition
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = false
        return transition
    }
    
    
}
