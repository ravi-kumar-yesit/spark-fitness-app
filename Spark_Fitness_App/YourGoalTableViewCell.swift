//
//  YourGoalTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 23/12/21.
//

import UIKit

class YourGoalTableViewCell: UITableViewCell {

    @IBOutlet weak var view_TableData: UIView!
    @IBOutlet weak var Label3: UILabel!
    @IBOutlet weak var Label2: UILabel!
    @IBOutlet weak var Label1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
