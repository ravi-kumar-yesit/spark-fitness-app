//
//  SubscriptionViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 28/12/21.
//

import UIKit

class SubscriptionViewController: UIViewController, UICollectionViewDataSource,UIScrollViewDelegate {
    

    @IBOutlet weak var myPage: UIPageControl!
    @IBOutlet weak var subscriptionCollectionView: UICollectionView!
    var arrayPlan1 = ["Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor."]
    var arrayPlan2 = ["Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor."]
    var arrayPlan3 = ["Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor."]
    var arrayPlan4 = ["Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor.",
    "Lorem ipsum dolor sit amet  dolor."]
    
    //var currentPage:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myPage.currentPage = 0
       myPage.numberOfPages = arrayPlan1.count
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.subscriptionCollectionView.contentOffset, size: self.subscriptionCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.subscriptionCollectionView.indexPathForItem(at: visiblePoint) {
            self.myPage.currentPage = visibleIndexPath.row
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//            return 0
//    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//
//        myPage.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: view.frame.size.width, height: subscriptionCollectionView.frame.size.height)
//    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        myPage.currentPage = indexPath.row
//
//    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayPlan1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = subscriptionCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SubscriptionCollectionViewCell
        cell.plan1.text = arrayPlan1[indexPath.row]
        cell.plan2Lbl.text = arrayPlan2[indexPath.row]
        cell.plan3Lbl.text = arrayPlan3[indexPath.row]
        cell.plan4Lbl.text = arrayPlan4[indexPath.row]
        //myPage.currentPage = indexPath.row
        return cell
        
    }
    

    @IBAction func CrossBtn_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SubscriptionViewController : UICollectionViewDelegateFlowLayout {

//   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//    return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {


                    return CGSize(width: (UIScreen.main.bounds.width - 370) / 1, height:  200)

                }


                func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
                 return 70 }

                func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
                    return 55      }

                    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
                           return UIEdgeInsets(top: 0, left: 35, bottom: 0, right: 0)
                    }
        }
    
    
   


 
