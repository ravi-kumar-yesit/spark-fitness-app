//
//  SettingViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 21/12/21.
//

import UIKit
import SideMenu
import SideMenuSwift

class SettingViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    
    //override func viewWillDisappear(_ animated: Bool) {
    
    
    // self.tabBarController?.tabBar.ishidden = false
    // viewController.hidesBottomBarWhenPushed = true
    //  }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func BtnBack_Action(_ sender: Any) {
        
        //        let vc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        //
        //        navigationController?.pushViewController(vc!, animated: false)
        
        self.tabBarController?.hidesBottomBarWhenPushed = false
        self.hidesBottomBarWhenPushed = false
        self.tabBarController?.tabBar.isHidden = false
       // self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func subscriptionBtn_Action(_ sender: Any) {
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "SubscriptionViewController") as! SubscriptionViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
        
    }
    @IBAction func preferrenceBtn_Action(_ sender: Any) {
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "PreferrenceViewController") as! PreferrenceViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    @IBAction func Btn_GoalsAction(_ sender: Any) {
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "GoalViewController") as! GoalViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
        
    }
    @IBAction func EditProfileBtn_Action(_ sender: Any) {
        
        
//        self.tabBarController?.hidesBottomBarWhenPushed = false
//        self.hidesBottomBarWhenPushed = false
//        self.tabBarController?.tabBar.isHidden = false
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
        //        self.navigationController?.popViewController(animated: true)
        
        
    }
    @IBAction func downloadsBtn_Action(_ sender: UIButton) {
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "DownloadsViewController") as! DownloadsViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    @IBAction func BtnNotification_Action(_ sender: Any) {
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    @IBAction func changePassword_Action(_ sender: Any) {
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        
        self.navigationController!.pushViewController(secondViewController, animated: true)
        
    }
    @IBAction func logOutBtn(_ sender: Any) {
       
        
        
        let alertController:UIAlertController = UIAlertController(title: "Alert", message: "Do you want to log out?", preferredStyle: UIAlertController.Style.alert)
        
        let alertAction:UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (action:UIAlertAction!) -> Void in

            self.performSegue(withIdentifier: "SignUpViewController", sender: self)

        })
        let alertAction1:UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler:nil)
        
        // Add alertAction object to alertController.
        alertController.addAction(alertAction)
        alertController.addAction(alertAction1)
        
        // Popup the alert dialog.
        present(alertController, animated: true, completion: nil)
        
            
//            UserDefaults.standard.set(false, forKey: "status")
//
//            AppDelegate.Switcher.updateRootVC()
            
        
//            
//            let leftMenuNavigationController = SideMenuNavigationController(rootViewController: HomeViewController?)
//            self.present(HomeViewController(), animated: true, completion: nil)
//            SideMenuManager.default.leftMenuNavigationController = leftMenuNavigationController
//          
//            SideMenuManager.default.addPanGestureToPresent(toView: self.navigationController!.navigationBar)
//            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
            
//            SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
//
//            SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
//            SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
//
//            var settings = SideMenuSettings()
//            SideMenuManager.default.rightMenuNavigationController?.settings = settings
            
            //self.navigationController?.popToRootViewController(animated: true)
            //let SignUpViewController =  UIStoryboard(name:K., bundle: <#T##Bundle?#>)
            
            //            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            
            //            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            //                         UserDefaults.standard.synchronize()
            //
            //                   let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            //
            //                   appDel.window?.rootViewController = loginVC
            
            //            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            //                UserDefaults.standard.synchronize()
            //
            //  let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            //self.navigationController!.pushViewController(loginVC, animated: true)
            
            //                let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            //
            //            appDel.window?.rootViewController = loginVC
            
            // appDel.window?.rootViewController = loginVC
            //            let VC = self.storyboard!.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            //
            //          self.tabBarController?.hidesBottomBarWhenPushed = true
            //            self.hidesBottomBarWhenPushed = true
            //            self.tabBarController?.tabBar.isHidden = true
            
            
            
            // exit(0) // to exist application
            
            //after user press ok, the following code will be execute
            // NSLog("User pressed OK!!")
        
    }
}
