//
//  QuickSearchCollectionViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 24/12/21.
//

import UIKit

class QuickSearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var exerciseTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
}
