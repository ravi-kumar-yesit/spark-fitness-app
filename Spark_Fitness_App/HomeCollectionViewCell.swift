//
//  HomeCollectionViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 16/12/21.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var homeImg: UIImageView!
}
