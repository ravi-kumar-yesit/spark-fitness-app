//
//  VerificaitonViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 14/12/21.
//

import UIKit

class VerificaitonViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var verification6_Txt: UITextField!
    @IBOutlet weak var verificaiton5_Txt: UITextField!
    @IBOutlet weak var verification4_Txt: UITextField!
    @IBOutlet weak var verification3_Txt: UITextField!
    @IBOutlet weak var verification2_Txt: UITextField!
    @IBOutlet weak var verfication1_Txt: UITextField!
    @IBOutlet weak var view_VerificaitonBtn: UIView!{didSet{
        view_VerificaitonBtn.layer.cornerRadius = view_VerificaitonBtn.layer.frame.height/2
    }}
    @IBOutlet weak var verificationBtn: UIButton!{didSet{
        verificationBtn.layer.cornerRadius = verificationBtn.layer.frame.height/2
    }}
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.verfication1_Txt.delegate = self
        self.verification2_Txt.delegate = self
        self.verification3_Txt.delegate = self
        self.verification4_Txt.delegate = self
        self.verificaiton5_Txt.delegate = self
        self.verification6_Txt.delegate = self
       
    
//        verification2_Txt.keyboardType = UIKeyboardType.numberPad
//        verification3_Txt.keyboardType = UIKeyboardType.numberPad
       
        verfication1_Txt.becomeFirstResponder()
    
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
//    func textField(_ shouldChangeCharactersIntextField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//
//            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
//
//            let components = string.components(separatedBy: inverseSet)
//
//            let filtered = components.joined(separator: "")
//
//            return string == filtered
//
//    }
func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        // Range.length == 1 means,clicking backspace
    
    if (range.length == 0){
        if textField == verfication1_Txt {
            
//            verfication1_Txt.keyboardType = UIKeyboardType.numberPad
            verification2_Txt?.becomeFirstResponder()
        }
        if textField == verification2_Txt {
//            verification2_Txt.keyboardType = UIKeyboardType.numberPad
            verification3_Txt?.becomeFirstResponder()
        }
        if textField == verification3_Txt {
            verification4_Txt?.becomeFirstResponder()
        }
        if textField == verification4_Txt {
            verificaiton5_Txt?.becomeFirstResponder()}
        
        if textField == verificaiton5_Txt {
            verification6_Txt?.becomeFirstResponder()
        /*After the otpbox4 is filled we capture the All the OTP textField and do the server call. If you want to capture the otpbox4 use string.*/
            _ = "\((verfication1_Txt?.text)!)\((verification2_Txt?.text)!)\((verification3_Txt?.text)!)\((verification4_Txt?.text)!)\((verificaiton5_Txt?.text)!)\((verification6_Txt?.text)!)\(string)"
        }
      
       
    
        //textField.text? = string
        textField.text? = string.filter("0123456789".contains)
        return false
    }else if (range.length == 1) {

            if textField == verification6_Txt {
                verificaiton5_Txt?.becomeFirstResponder()
            }
            if textField == verificaiton5_Txt {
                verification4_Txt?.becomeFirstResponder()
            }
            if textField == verification4_Txt {
                verification3_Txt?.becomeFirstResponder()
            }
            if textField == verification3_Txt {
                verification2_Txt?.resignFirstResponder()
            }
        if textField == verification2_Txt {
            verfication1_Txt?.resignFirstResponder()
        }
        if textField == verfication1_Txt {
            verfication1_Txt?.resignFirstResponder()
        }
            textField.text? = ""
            return false
    }
    return true
    }



func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    verfication1_Txt.resignFirstResponder()
    verification2_Txt.resignFirstResponder()
    verification3_Txt.resignFirstResponder()
    verification4_Txt.resignFirstResponder()
    verificaiton5_Txt.resignFirstResponder()
    verification6_Txt.resignFirstResponder()
    return true
}
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == verfication1_Txt {
//                    let allowedCharacters = "1234567890"
//                    let allowedCharacterSet = CharacterSet(charactersIn: allowedCharacters)
//                    let typedCharacterSet = CharacterSet(charactersIn: string)
//                    let alphabet = allowedCharacterSet.isSuperset(of: typedCharacterSet)
//                    return alphabet
//
//
//        }}
//    func textField(_ textField: UITextField, shouldChangeCharactersIn
//      range: NSRange, replacementString string: String) -> Bool {
//
//        if textField.text?.count == 0 && string == "0" {
//            return false
//        }
//        return string == string.filter("0123456789".contains)
//    }
    @IBAction func verificationBtn_Action(_ sender: Any) {
        
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "OnBoarding1ViewController") as! OnBoarding1ViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    @IBAction func back_Btn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}


    
