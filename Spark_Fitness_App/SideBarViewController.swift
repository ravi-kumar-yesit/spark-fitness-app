//SideBarViewController
//  SideBarViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 16/12/21.
//

import UIKit
//import SideMenu

class SideBarViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    let transition = slideTransition()
    
    var timer = Timer()
    var currentcellIndex = 0
    var isSideViewOpen: Bool = false
    
    @IBOutlet weak var sidebartableview: UITableView!
    
    var SidebarArray = ["Home","About Us","Fitness","My Spark Fitness Feed","Settings","Term & Condition","Privacy Policy"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        sidebartableview.dataSource = self
        sidebartableview.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    @IBAction func action_close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func HideBtn(_ sender: UIButton) {
        //        self.view.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
        //        NotificationCenter.default.post(name:  Notification.Name(rawValue: "drawer"), object: nil)
    }
    
    //    @IBAction func closesidebtn_Action(_ sender: Any) {
    //
    ////
    ////        self.view.removeFromSuperview()
    ////                self.dismiss(animated: true, completion: nil)
    //
    ////            let transition = CATransition()
    ////                    transition.duration = 0.5
    ////                    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    ////                    transition.type = CATransitionType.reveal
    ////                    transition.subtype = CATransitionSubtype.fromRight
    ////                    self.view.window!.layer.add(transition, forKey: nil)
    ////
    ////                    self.view.removeFromSuperview()
    ////                            self.dismiss(animated: true, completion: nil)
    //
    //    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        SidebarArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = sidebartableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SidebarTableViewCell
        cell2.sidebarLbl.text = SidebarArray[indexPath.row]
        if indexPath.row == 2{
            cell2.imgSideBarBig.image = UIImage(named: SidebarArray[indexPath.row])
            cell2.imgSideBar.isHidden = true
            cell2.imgSideBarBig.isHidden = false
        }else{
            cell2.imgSideBar.image = UIImage(named: SidebarArray[indexPath.row])
            cell2.imgSideBarBig.isHidden = true
            cell2.imgSideBar.isHidden = false
        }
        return cell2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = ["object":indexPath.row]
        NotificationCenter.default.post(name:  Notification.Name(rawValue: "drawer"), object: dict)
        self.dismiss(animated: true, completion: nil)
        //        switch (indexPath.row) {
        //
        //                case 0:
        //                     let viewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        //                    self.navigationController?.pushViewController(viewController, animated: true)
        //                 case 1:
        
        //                    let vc: AboutUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        //
        //                                      self.addChild(vc)
        //
        //                                      vc.view.frame = self.view.frame
        //                                      self.view.addSubview(vc.view)
        //                                      vc.didMove(toParent: self)
        
        
        //
        //                     let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
        //                    self.navigationController?.pushViewController(viewController, animated: true)
        //                case 2:
        //                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "FitnessViewController") as! FitnessViewController
        //                    self.navigationController?.pushViewController(viewController, animated: true)
        //                case 3:
        //                     let viewController = self.storyboard?.instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
        //                    self.navigationController?.pushViewController(viewController, animated: true)
        //               case 4:
        //                     let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        //                self.navigationController?.pushViewController(viewController, animated: true)
        //               case 6:
        //                       let viewController = self.storyboard?.instantiateViewController(withIdentifier: "TermConditionViewController") as! TermConditionViewController
        //                self.navigationController?.pushViewController(viewController, animated: true)
        //              case 7:
        //                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
        //                self.navigationController?.pushViewController(viewController, animated: true)
        //                default:
        //                       print("default")
        //              }
        
        
    }
    
    //        let transition = CATransition()
    //             transition.duration = 0.5
    //             transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    //             transition.type = CATransitionType.reveal
    //             transition.subtype = CATransitionSubtype.fromRight
    //             self.view.window!.layer.add(transition, forKey: nil)
    //
    //             self.view.removeFromSuperview()
    //                     self.dismiss(animated: true, completion: nil)
    //             self.dismiss(animated: false, completion: nil)
}

//extension SideBarViewController: UIViewControllerTransitioningDelegate{
//
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//
//        transition.isPresenting = false
//        return transition
//    }
//}



extension SideBarViewController: UIViewControllerTransitioningDelegate{
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = false
        return transition
    }
}
