//
//  VedioDescriptionModeViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 21/12/21.
//

import UIKit
import FSCalendar
import AVFoundation
import AVKit

class VedioDescriptionModeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,FSCalendarDelegate,UIScrollViewDelegate {
  
    @IBOutlet weak var PageControll1: UIPageControl!
    @IBOutlet weak var scrollViewImg: UIScrollView!
    
   // var imgArrayPageControl = ["Rectangle 42","Rectangle 42","Rectangle 42","Rectangle 42"]
    @IBOutlet weak var collect: UICollectionView!
    var playerController = AVPlayerViewController()
    
    @IBOutlet weak var view_DescriptionModeContent: UIView!
    @IBOutlet weak var LogBtn: UIButton!
    @IBOutlet weak var descriptionTableView: UITableView!
    // array Description Mode
    
    @IBOutlet weak var imgDescriptionExerciseIcon: UIImageView!
   // var imgDescriptionExerciseIcon1 = UIImage()
    
    //vedio image exercise
    @IBOutlet weak var imgExercise: UIImageView!
   // var image1 = UIImage()
    //image vedio exercise icon
    
    @IBOutlet weak var imgExerciseIcon: UIImageView!
    
   // var imageExerciseIconArray = UIImage()
    
    @IBOutlet weak var imgExercise1: UIImageView!
  //  var image2 = UIImage()
    var desctitleArray = ["A) Strength x3","B) Cardio x2"]
    var exerciseDescArray1 = ["Reverse lunge + Squat (right)","Ball slams (forward)"]
    var exerciseDescArray2 = ["Reverse lunge + Squat (left)","Ball slams (right side)"]
    var exerciseDescArray3 = ["Curtsy lunge + Deadlift (right)","Ball slams (left side)"]
    var exerciseDescArray4 = ["Curtsy lunge + Deadlift (left)"," Burpee hop overs"]
    
    @IBOutlet weak var star10: UIButton!
    @IBOutlet weak var star9: UIButton!
    @IBOutlet weak var star8: UIButton!
    @IBOutlet weak var star7: UIButton!
    @IBOutlet weak var star6: UIButton!
    @IBOutlet weak var star5: UIButton!
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star1: UIButton!
    @IBOutlet weak var view_VedioMode: UIView!
    @IBOutlet weak var btnDescriptionMode: UIButton!{didSet{
        btnDescriptionMode.layer.cornerRadius = btnDescriptionMode.layer.frame.height/2
        btnDescriptionMode.clipsToBounds = true
    }}
    @IBOutlet weak var btnVedioMode: UIButton!{didSet{
        btnVedioMode.layer.cornerRadius = btnVedioMode.layer.frame.height/2
        btnVedioMode.clipsToBounds = true
    }}
    // array Vedio Mode
    @IBOutlet weak var vediodesciptionmodetableview: UITableView!
    var nameArray = ["Michael","Robert","Robert"]
    var timeArray = ["2 days ago","3 days ago","3 days ago"]
    var imageArray = ["Ellipse 8","Ellipse 8","Ellipse 8"]
    var descriptionArray = ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula","Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula"]
   // var IsDescriptionboolean = true
    
    var dataDict:[String:Any]?
    
    var delegate:DelegatefoLogPace? = nil
    var delegate1:DelegatefoLogWeight? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //PageControll1.numberOfPages = imgArrayPageControl.count
        
//        for index in 0..<imgArrayPageControl.count {
//            let imageView = UIImageView()
//            imageView.contentMode = .scaleToFill
//            imageView.image = UIImage(named: imgArrayPageControl[index])
//            let xPos =  CGFloat(index)*self.view.bounds.size.width
//            imageView.frame = CGRect(x: xPos, y: 0, width: view.frame.size.width-10, height: scrollViewImg.frame.size.height)
//            scrollViewImg.contentSize.width = view.frame.size.width*CGFloat(index+1)
//            scrollViewImg.addSubview(imageView)
//
      //  }

        imgExercise.image = UIImage(named: (dataDict?["image"] as? String)!)
      
       // imgExercise1.image = UIImage(named: (dataDict?["image"] as? String)!)
        
        imgDescriptionExerciseIcon.image = UIImage(named: (dataDict?["imageIcon"] as? String)!)
        imgExerciseIcon.image = UIImage(named: (dataDict?["imageIcon"] as? String)!)
        

        
        if dataDict!["name"] as! String == "Cardio" {
            
            LogBtn.setTitle("Log Pace", for: .normal) }
      
            else {
                LogBtn.setTitle("Log Weight", for: .normal)
            }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollViewImg.contentOffset.x/scrollViewImg.frame.width
        PageControll1.currentPage = Int(page)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
//    func playVedio(){
//
//        guard let url = URL(string: "https://www.pexels.com/video/6650177/download/") else { return}
//        let player = AVPlayer(url: url)
//        playerController = AVPlayerViewController()
//
//        playerController.
//
//    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func PlayVedio(){
        let vedioUrl = URL(string:"https://assets.mixkit.co/videos/download/mixkit-girl-doing-sit-ups-lying-on-the-floor-4591.mp4")
        let player = AVPlayer(url: vedioUrl! )
          let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        playerLayer.videoGravity = .resize

        self.view.layer.addSublayer(playerLayer)
        player.play()

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == vediodesciptionmodetableview {
            return nameArray.count
        }
        else {
            return desctitleArray.count
        }
        
        //    }
        //        if IsDescriptionboolean == false{
        //           // IsDescriptionboolean = false
        //            return nameArray.count
        //
        //        }
        //        else {
        //           // IsDescriptionboolean = true
        //             return desctitleArray.count
        //
        ////            return nameArray.count
        //        }
        
    }
    @IBAction func vedioBtn_Action(_ sender: Any) {
        
        let player = AVPlayer(url: URL(string: "https://assets.mixkit.co/videos/download/mixkit-girl-doing-sit-ups-lying-on-the-floor-4591.mp4")!)
              let vc = AVPlayerViewController()
              vc.player = player

              present(vc, animated: true) {
                  vc.player?.play()
              }
        
        // Play video in small view
        
//        let vc: PlayVedioViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlayVedioViewController") as! PlayVedioViewController
//
//        self.addChild(vc)
//
//        vc.view.frame = self.view.frame
//        self.view.addSubview(vc.view)
//        self.view.bringSubviewToFront(vc.view)
//        vc.didMove(toParent: self)
        
//        PlayVedio()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == vediodesciptionmodetableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VedioDescriptionModeTableViewCell
            cell.nameLbl.text = nameArray[indexPath.row]
            cell.descriptionLbl.text = descriptionArray[indexPath.row]
            cell.imgProfile.image = UIImage(named: imageArray[indexPath.row])
            cell.timeLbl.text = timeArray[indexPath.row]
            
            // IsDescriptionboolean = false
            
            return cell
        }
        
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DescriptionTableViewCell
            cell.descriptionTitle.text = desctitleArray[indexPath.row]
            cell.descLbl1.text = exerciseDescArray1[indexPath.row]
            cell.descLbl2.text = exerciseDescArray2[indexPath.row]
            cell.descLbl3.text = exerciseDescArray3[indexPath.row]
            cell.descLbl4.text = exerciseDescArray4[indexPath.row]
            //IsDescriptionboolean = true
            cell.view_TableDataStack.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            return cell
        }
        
    }
        ///asdfasd
        //        if IsDescriptionboolean == false {
        //        let cell = vediodesciptionmodetableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VedioDescriptionModeTableViewCell
        //        cell.nameLbl.text = nameArray[indexPath.row]
        //        cell.descriptionLbl.text = descriptionArray[indexPath.row]
        //        cell.imgProfile.image = UIImage(named: imageArray[indexPath.row])
        //        cell.timeLbl.text = timeArray[indexPath.row]
        //           // IsDescriptionboolean = false
        //
        //            return cell
        //        }
        //
        //        else {
        //            let cell = descriptionTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DescriptionTableViewCell
        //            cell.descriptionTitle.text = desctitleArray[indexPath.row]
        //            cell.descLbl1.text = exerciseDescArray1[indexPath.row]
        //            cell.descLbl2.text = exerciseDescArray2[indexPath.row]
        //            cell.descLbl3.text = exerciseDescArray3[indexPath.row]
        //            cell.descLbl4.text = exerciseDescArray4[indexPath.row]
        //            //IsDescriptionboolean = true
        //
        //            return cell
        //        }
        
    
    @IBAction func LogBtn_Action(_ sender: Any) {
        if LogBtn.currentTitle == "Log Pace" {
        let vc: LogPaceViewController = self.storyboard?.instantiateViewController(withIdentifier: "LogPaceViewController") as! LogPaceViewController
            vc.delegate2 = self
            
        self.addChild(vc)
        
        vc.view.frame = self.view.frame
        self.view.addSubview(vc.view)
        self.view.bringSubviewToFront(vc.view)
        vc.didMove(toParent: self)
        }
        else {
            if LogBtn.currentTitle == "Log Weight" {
                
            let vc: LogWeightViewController = self.storyboard?.instantiateViewController(withIdentifier: "LogWeightViewController") as! LogWeightViewController
                vc.delegate1 = self
            self.addChild(vc)
            
            vc.view.frame = self.view.frame
            self.view.addSubview(vc.view)
            self.view.bringSubviewToFront(vc.view)
            vc.didMove(toParent: self)
            }
        }
    }
    @IBAction func Btn_vedioModeAction(_ sender: Any) {
        btnVedioMode.backgroundColor = UIColor.orange
        btnDescriptionMode.backgroundColor = hexStringToUIColor(hex: "151720")
        view_VedioMode.isHidden = false
        view_DescriptionModeContent.isHidden = true
//        if IsDescriptionboolean == false {
//            IsDescriptionboolean = true
//
//        }
        
    }
    @IBAction func Btn_DescriptionModeAction(_ sender: Any) {
        btnVedioMode.backgroundColor = hexStringToUIColor(hex: "151720")
        btnDescriptionMode.backgroundColor = UIColor.orange
        view_VedioMode.isHidden = true
        
        view_DescriptionModeContent.isHidden = false
//        if IsDescriptionboolean == true {
//            IsDescriptionboolean = false
//            
//        }
        
    }
    
    @IBAction func Btnstar1_Action(_ sender: Any) {
        if star1.isSelected  == false {
            star1.isSelected = true
            
        } else {
            star1.isSelected = false
        }
        
    }
    @IBAction func Btnstar2_Action(_ sender: Any) {
        if star2.isSelected  == false {
            star1.isSelected = true
            star2.isSelected = true
            
            
        } else {
            star2.isSelected = false
        }
    }
    @IBAction func btnstar3_Action(_ sender: Any) {
        
        if star3.isSelected  == false {
            star1.isSelected = true
            star2.isSelected = true
            star3.isSelected = true
            
        } else {
            star3.isSelected = false
        }
    }
    @IBAction func btnstar4_Action(_ sender: Any) {
        if star4.isSelected  == false {
            star1.isSelected = true
            star2.isSelected = true
            star3.isSelected = true
            star4.isSelected = true
            
        } else {
            star4.isSelected = false
        }
        
    }
   
    @IBAction func btnStar6Action(_ sender: Any) {
        if star6.isSelected == false {
            star6.isSelected = true
        } else {
            star6.isSelected = false
        }
    }
    @IBAction func btnStar7Action(_ sender: Any) {
        if star7.isSelected == false {
            star7.isSelected = true
        } else {
            star7.isSelected = false
        }
    }
    @IBAction func btnStar8Action(_ sender: Any) {
        if star8.isSelected == false {
            star8.isSelected = true
        } else {
            star8.isSelected = false
        }
        
    }
    @IBAction func btnStar9Action(_ sender: Any) {
        if star9.isSelected == false {
            star9.isSelected = true
        } else {
            star9.isSelected = false
        }
    }
    
    @IBAction func btnStar10Action(_ sender: Any) {
        if star10.isSelected == false {
            star10.isSelected = true
        } else {
            star10.isSelected = false
        }
        
    }
    @IBAction func btnStar5_Action(_ sender: Any) {
        if star5.isSelected  == false {
            star1.isSelected = true
            star2.isSelected = true
            star3.isSelected = true
            star4.isSelected = true
            star5.isSelected = true
            
        } else {
            star5.isSelected = false
        }
        
    }
    @IBAction func backBtn_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print(date)
    }
}
extension VedioDescriptionModeViewController: DelegatefoLogPace,DelegatefoLogWeight {
    func MethodforPop() {
        
//        let vc: AddNewGoalViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddNewGoalViewController") as! AddNewGoalViewController
    
//        vc.view.removeFromSuperview()
        
        if self.children.count > 0 {
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                viewContoller.willMove(toParent: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParent()
                }
            }
    }
}
