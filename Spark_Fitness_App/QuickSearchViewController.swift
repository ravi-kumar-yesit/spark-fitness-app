//
//  QuickSearchViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 24/12/21.
//

import UIKit

class QuickSearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    var exerciseArray = ["Strength","HIIT","Cardio","Mobility"]
    @IBOutlet weak var quickSearchCollectionView: UICollectionView!
    @IBOutlet weak var searchTxt: UITextField!{didSet {
    let redPlaceholderText = NSAttributedString(string: "Search",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        searchTxt.attributedPlaceholder = redPlaceholderText
}
}
    @IBOutlet weak var view_SearchText: UIView!
    {didSet{
        view_SearchText.layer.cornerRadius = view_SearchText.layer.frame.height/2
        view_SearchText.layer.borderWidth = 0.01
        view_SearchText.layer.borderColor = UIColor.lightGray.cgColor
    }}
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let width = (view.frame.width-40)/3
//                        let layout = quickSearchCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//                layout.itemSize = CGSize(width: width, height: 150)

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return exerciseArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell1 = quickSearchCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! QuickSearchCollectionViewCell
        cell1.exerciseTitle.text = exerciseArray[indexPath.row]
        cell1.img.image = UIImage(named: exerciseArray[indexPath.row])
        return cell1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = ((collectionView.frame.size.width))/2
        return CGSize(width: size-5, height: size)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 5.0, bottom: 10.0, right: 5.0)//here your custom value for spacing

}

    
}
