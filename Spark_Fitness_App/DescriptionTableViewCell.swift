//
//  DescriptionTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 21/12/21.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var view_TableDataStack: UIStackView!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descLbl1: UILabel!
    @IBOutlet weak var descLbl2: UILabel!
    @IBOutlet weak var descLbl3: UILabel!
    @IBOutlet weak var descLbl4: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
