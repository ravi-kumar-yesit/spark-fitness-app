//
//  AnotherUserProfileTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 22/12/21.
//

import UIKit

class AnotherUserProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var profileDescriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
