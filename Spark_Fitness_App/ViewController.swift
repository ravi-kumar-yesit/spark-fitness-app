//
//  ViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 09/12/21.
//

import UIKit
import SideMenu

class ViewController: UIViewController {
    
    @IBOutlet weak var password_Txt: UITextField!{
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Password",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            
            password_Txt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var emailPhone_Txt: UITextField!{
        didSet {
            let redPlaceholderText = NSAttributedString(string: "Email/Phone",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            
            emailPhone_Txt.attributedPlaceholder = redPlaceholderText
        }
    }
    @IBOutlet weak var login_Btn: UIButton!
    {didSet{
        login_Btn.layer.cornerRadius = login_Btn.layer.frame.height/2
        login_Btn.layer.borderWidth = 1
        login_Btn.layer.borderColor = UIColor.gray.cgColor
        
    }
    }
    
    @IBOutlet weak var view_PasswordTxt: UIView!{didSet{
        
        view_PasswordTxt.layer.cornerRadius = view_PasswordTxt.layer.frame.height/2
        view_PasswordTxt.layer.borderWidth = 1
        view_PasswordTxt.layer.borderColor = UIColor.gray.cgColor
    }
    }
    @IBOutlet weak var view_EmailPhoneTxt: UIView!{didSet{
        view_EmailPhoneTxt.layer.cornerRadius = view_EmailPhoneTxt.layer.frame.height/2
        view_EmailPhoneTxt.layer.borderWidth = 1
        view_EmailPhoneTxt.layer.borderColor = UIColor.gray.cgColor
    }}
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.hidesBottomBarWhenPushed = true
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }
    func isValidEmail(yourEmail: String)-> Bool{
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: yourEmail)
    }
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
                                       "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}")
        //        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
        //       "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,30}")
        return passwordTest.evaluate(with: testStr)
    }
    func isValidPhone(phone: String) -> Bool {
        let phoneRegex = "^[0-9+]{8,10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phone)
    }
    @IBAction func forgotBtn_Action(_ sender: Any) {
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
    }
    @IBAction func Btn_Login(_ sender: Any) {
        
        if emailPhone_Txt.text!.isEmpty  {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter correct email/phone number", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
        }
        if !isValidEmail(yourEmail: emailPhone_Txt.text!) && !isValidPhone(phone: emailPhone_Txt.text!){
            let alert = UIAlertController(title: "Alert", message: "Please enter correct email or phone number", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true,completion: {return})
            
        }
        if !isValidPassword(testStr: password_Txt.text!) {
            
            let alert = UIAlertController(title: "Alert", message: "Please enter atleast 6 digit (Alpha + Numeric)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            present(alert, animated: true,completion: {return})
            
        }
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        
        self.navigationController!.pushViewController(secondViewController, animated: true)
        
        
        //        self.navigationController!.pushViewController(leftMenuNavigationController, animated: true)
        //        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        //        self.navigationController!.pushViewController(secondViewController, animated: true)
        
        
    }
    @IBAction func signUpBtn_Action(_ sender: Any) {
        
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
        
    }
}

