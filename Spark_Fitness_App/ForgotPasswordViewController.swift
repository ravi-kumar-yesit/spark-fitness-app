//
//  ForgotPasswordViewController.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 15/12/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailPhoneTxt: UITextField!{didSet{
        emailPhoneTxt.attributedPlaceholder = NSAttributedString(string: "Enter Registered Email/Phone", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
     }}
    @IBOutlet weak var sendVerificationBtn: UIButton!{didSet{
        sendVerificationBtn.layer.cornerRadius = sendVerificationBtn.layer.frame.height/2
    }}
    @IBOutlet weak var view_emailPhone: UIView!{didSet{
        view_emailPhone.layer.cornerRadius = view_emailPhone.layer.frame.height/2
    }}
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return.lightContent
        
    }

    func isValidEmail(yourEmail: String)-> Bool{
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
                   let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: yourEmail)
    }
    
    func isValidPhone(phone: String) -> Bool {
            let phoneRegex = "^[0-9+]{10}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return phoneTest.evaluate(with: phone)
        }
    
    @IBAction func sendVerificationBtn_Action(_ sender: Any) {
       
        if emailPhoneTxt.text!.isEmpty  {
        
                    let alert = UIAlertController(title: "Alert", message: "Please enter email/phone number", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alert, animated: true, completion: {return})
                }
        if !isValidEmail(yourEmail: emailPhoneTxt.text!) && !isValidPhone(phone: emailPhoneTxt.text!){
                let alert = UIAlertController(title: "Alert", message: "Please enter correct email/phone number", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                present(alert, animated: true,completion: {return})
               
        }
//        if isValidPhone(phone: emailPhoneTxt.text!) == true && isValidEmail(yourEmail: emailPhoneTxt.text!) == true {
        let secondViewController = self.storyboard!.instantiateViewController(withIdentifier: "VerificaitonforgotViewController") as! VerificaitonforgotViewController
        self.navigationController!.pushViewController(secondViewController, animated: true)
//        }
    }
    @IBAction func back_Btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
