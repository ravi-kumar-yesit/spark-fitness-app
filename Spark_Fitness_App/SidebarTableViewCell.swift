//
//  SidebarTableViewCell.swift
//  Spark_Fitness_App
//
//  Created by pranjali kashyap on 16/12/21.
//

import UIKit

class SidebarTableViewCell: UITableViewCell {

    @IBOutlet weak var imgSideBarBig: UIImageView!
    @IBOutlet weak var imgSideBar: UIImageView!
    @IBOutlet weak var sidebarLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
